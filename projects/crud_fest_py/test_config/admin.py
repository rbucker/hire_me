__author__ = 'rbucker'

from django.contrib import admin
from crud_fest_py.test_config.models import MessageRequest, MessageResponse, MessageFieldDictionary, MessageTestCases, MessageTestResults, TestCards

class MessageFieldDictionaryAdmin(admin.ModelAdmin):
    pass

class MessageRequestAdmin(admin.ModelAdmin):
    pass

class MessageResponseAdmin(admin.ModelAdmin):
    pass

class MessageTestCasesAdmin(admin.ModelAdmin):
    pass

class MessageTestResultsAdmin(admin.ModelAdmin):
    pass

class TestCardsAdmin(admin.ModelAdmin):
    pass

admin.site.register(MessageFieldDictionary, MessageFieldDictionaryAdmin)
admin.site.register(MessageRequest, MessageRequestAdmin)
admin.site.register(MessageResponse, MessageResponseAdmin)
admin.site.register(MessageTestCases, MessageTestCasesAdmin)
admin.site.register(MessageTestResults, MessageTestResultsAdmin)
admin.site.register(TestCards, TestCardsAdmin)
