# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class MessageFieldDictionary(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    field_id = models.IntegerField(unique=True, null=True, blank=True)
    field_name = models.CharField(unique=True, max_length=25, blank=True)
    field_description = models.TextField(blank=True)
    field_format = models.CharField(max_length=200, blank=True)
    data_types = models.CharField(max_length=200, blank=True)
    default_values = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = u'message_field_dictionary'

class MessageRequest(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    test_case_id = models.IntegerField(null=True, blank=True)
    field_id = models.IntegerField(null=True, blank=True)
    field_value = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = u'message_request'

class MessageResponse(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    test_case_id = models.IntegerField(null=True, blank=True)
    field_id = models.IntegerField(null=True, blank=True)
    field_value = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = u'message_response'

class MessageTestCases(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    test_case_id = models.IntegerField(unique=True, null=True, blank=True)
    short_name = models.CharField(unique=True, max_length=100, blank=True)
    description = models.TextField(blank=True)
    elapsed_ceiling = models.IntegerField(null=True, blank=True)
    is_active = models.NullBooleanField(null=True, blank=True)
    group_name = models.CharField(max_length=100, blank=True)
    sub_group_name = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = u'message_test_cases'

class MessageTestResults(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    test_case_id = models.IntegerField(null=True, blank=True)
    started = models.TextField(blank=True) # This field type is a guess.
    finished = models.TextField(blank=True) # This field type is a guess.
    elapsed_time = models.IntegerField(null=True, blank=True)
    results = models.CharField(max_length=100, blank=True)
    request = models.TextField(blank=True)
    response = models.TextField(blank=True)
    errors = models.TextField(blank=True)
    trace = models.TextField(blank=True)
    class Meta:
        db_table = u'message_test_results'

class TestCards(models.Model):
    id = models.IntegerField(null=True, primary_key=True, blank=True)
    serial_number = models.IntegerField(unique=True)
    card_number = models.CharField(max_length=19)
    expiration_date = models.CharField(max_length=4, blank=True)
    issue_date = models.TextField(blank=True) # This field type is a guess.
    street = models.CharField(max_length=200, blank=True)
    zipcode = models.CharField(max_length=10, blank=True)
    pin = models.CharField(max_length=6, blank=True)
    atm_pin = models.CharField(max_length=6, blank=True)
    cvv = models.CharField(max_length=6, db_column=u'CVV', blank=True) # Field name made lowercase.
    cvv2 = models.CharField(max_length=6, db_column=u'CVV2', blank=True) # Field name made lowercase.
    track1 = models.CharField(max_length=150, blank=True)
    track2 = models.CharField(max_length=150, blank=True)
    track3 = models.CharField(max_length=150, blank=True)
    reset_balance = models.NullBooleanField(null=True, blank=True)
    is_decrement = models.NullBooleanField(null=True, blank=True)
    actual_balance = models.TextField(blank=True) # This field type is a guess.
    open_to_buy = models.TextField(blank=True) # This field type is a guess.
    class Meta:
        db_table = u'test_cards'

