#!/usr/bin/env perl
use strict;

#
# Copyright (c) 2011, Richard Bucker <richard@bucker.net>
# All rights Reserved
#

=comment
This module is a worker module. It is waiting for message to arrive.
When the application starts it is going to bind to a well known IP and 
host. It is then going to listen to a particular channel foe messages
to arrive. Ast they arrive they will be processed and a response returned.
The return channel name is the UUID that was provided in the request.

1) bind to the beanstalk port
2) wait for messages to arrive
3) when the message arrives parse the UUID
4) open the response channel
5) get the message data from redis based on the UUID
6) what whatever the request was
7) put the data back into redis
8) send the UUID back to the requester via the UUID channel
=cut

use JSON::XS;
use UUID::Tiny;
use Data::Dumper;
use AnyEvent::Beanstalk;


sub process_job($) {
  my ($job) = @_;
  printf "data> %s\n", $job->data;
};

my $client = AnyEvent::Beanstalk->new();
$client->encoder(\&JSON::XS::encode_json);
$client->decoder(\&JSON::XS::decode_json);

$client->list_tubes(sub {my $tubes = shift; print "tubes: ".Dumper($tubes)});
$client->list_tube_used(sub {my $tube = shift; print "used: ".Dumper($tube)});
$client->list_tubes_watched(sub {my $tubes = shift; print "watched: ".Dumper($tubes)});
$client->sync;


$client->use('restmq');

#my @data = ['hello','world'];
my $guid = create_UUID_as_string(UUID_V1);
my @data = [{hello=>'hello', uuid=>$guid}];

my $i = 10;
while ($i) {
  my $job = undef;
  my $job2 = $client->put(
    { priority => 100,
      ttr      => 120,
      delay    => 0,
      encode   => \@data,
    },
  )->recv;
  $i--;
}

sub process_job($) {
  my ($job) = @_;
  if ($job) {
     my $decode = $job->decode;
     my $retval = (@$decode)[0][0]->{'uuid'};
     printf "data> %s, %s\n", $job->data, $retval;
     return $retval;
  }
  return undef;
};

$client->watch($guid);
while (1) {
  my $job = undef;
  my $retval = undef;
  $client->reserve( sub { $job = shift; $retval = process_job($job) })->recv;  
  $client->delete($job->id);
}

__END__
