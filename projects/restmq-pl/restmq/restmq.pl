#!/usr/bin/env perl
use strict;

#
# Copyright (c) 2011, Richard Bucker <richard@bucker.net>
# All rights Reserved
#

=comment
This module is going to be started from the command line. The usage is
described later. This module is implemented as a Mojolicious lite app.
and will bind to the default host IP and port. From there the app
will wait for connections to be make by external browsers or app where
the user app is going to send a message. This message is going to be
some REST message that the web app is going to process as follows:

1) respond to the connection request (mojo)
2) parse the remote request
3) create a UUID for this transaction
4) save the contents to redis with the UUID as the primary key
5) use the UUID as the reverse channel to watch for responses
6) send the request to besnstalkd and wait for a reply
**) since everything is implemented using AnyEvent it should be
able to process some decent volume.
=cut

use EV;
use AnyEvent;
use JSON::XS;
use UUID::Tiny;
use Data::Dumper;
use Mojolicious::Lite;
use AnyEvent::Beanstalk;


get '/' => sub {
  my $self = shift;
  my $client = AnyEvent::Beanstalk->new();
  $client->encoder(\&JSON::XS::encode_json);
  $client->decoder(\&JSON::XS::decode_json);

  my $guid = create_UUID_as_string(UUID_V1);
  my @data = [{hello=>'hello', uuid=>$guid}];
  my $msg = 
    { priority => 100,
      ttr      => 120,
      delay    => 0,
      encode   => \@data,
    };

  my $txn = AnyEvent::condvar;
  $txn->begin;
  $client->use('restmq', sub {$txn->end;});
  $txn->begin;
  $client->watch($guid, sub {$txn->end;});
  $txn->cb(sub { $_[0]->recv });
  
  $txn = AnyEvent::condvar;
  $txn->begin;
  my $job2 = $client->put( $msg, sub {$txn->end;});
  $txn->begin;
  $client->reserve( sub { my $job = shift; 
                          my $decode = $job->decode;
                          my $uuid = (@$decode)[0][0]->{'uuid'};
                          my $retval = sprintf "data> %s, %s\n", $job->data, $uuid;
                          $client->delete($job->id);
                          $client->ignore($guid);
                          $client->quit;
                          $self->render(text => $retval);
                          $txn->end;
                        });  
  $txn->cb(sub { $_[0]->recv });
 
  $self->render_later;
};

app->start;

__END__

