#!/usr/bin/env perl
use Mojolicious::Lite;
use EV;
use AnyEvent;

# Simple delayed rendering
get '/' => sub {
  my $self = shift;
  my $w;
  $w = AE::timer 3, 0, sub {
    $self->render_text('3 seconds delayed!');
    undef $w;
  };
  $self->render_later;
};

app->start;
