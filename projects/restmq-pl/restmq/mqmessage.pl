#
# Copyright (c) 2011, Richard Bucker <richard@bucker.net>
# All rights Reserved
#
package mqmessage;

use redis::AnyEvent;

:<<COMMENT
This is a helper module that is going to provide helper functions in order
to store and retrieve data from redis. This is essentially the model 
comnponent in the MVC.

This class is going to be a wrapper around a hash data structure and the
redis key/value structure.
COMMENT

__END__
