#!/usr/bin/env perl
use Mojolicious::Lite;
use EV;
use AnyEvent;

# Simple delayed rendering
get '/' => sub {
  my $self = shift;
    $self->render_text('3 seconds delayed!');
};

app->start;
