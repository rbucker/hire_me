#!/usr/bin/env perl
# Mojolicious minimalist RestMQ
# no COMET, just /q/ routes and queue logic
# the core of RestMQ is how it uses Redis' data types

use Mojolicious::Lite;
use MojoX::Redis;
use JSON;

my $QUEUESET     = 'QUEUESET';   # queue index
my $UUID_SUFFIX  = ':UUID';      # queue unique id
my $QUEUE_SUFFIX = ':queue';     # suffix to identify each queue's LIST

my $redis = MojoX::Redis->new(server => '127.0.0.1:6379', ioloop => Mojo::IOLoop->new);

get '/q' => sub {
  my $self = shift;
  my $result = undef;
  ($redis, $result) = $redis->execute("smembers" => [$QUEUESET] => sub{ return @_; });
  if (! defined $result || ! $result == 0) {
    $self->render_not_found;
  } else {
    #b.map! do |q| q = '/q/'+q end
    my $json = JSON->new->allow_nonref;
    $self->render(text => $json->pretty->encode($result))
  }
};

post '/q/:queue' => sub {
  my $self = shift;
  my $result = undef;
  my $queue  = $self->param('queue');
  my $value = $self->param('value');
  if (! defined $queue) {
    $self->app->log->debug('queue was not in the URL ('.$queue.')');
    $self->render_not_found;
  } else {
    my $uuid = undef;
    my $lkey = undef;
    my $q1   = $queue . $QUEUE_SUFFIX;
    my $q2   = $queue . $UUID_SUFFIX;
    $redis->incr($q2 => sub{ 
                             my ($redis, $res) = @_; 
                             $uuid = $res->[0]; 
                             $redis->ioloop->stop;
                            })->start;
    $self->app->log->debug('the uuid is ('.($uuid||'undefined').')');
    $lkey = $queue . ':' . $uuid;
    $redis->execute("sadd" => [$QUEUESET, $q1]);
    $redis->execute("set" => [$lkey, $value]);
    $redis->execute("lpush" => [$q1, $lkey]);
    $self->app->log->debug('the uuid q is ('.($q2||'undefined').')');
    $self->render(text => '{ok, ' . $lkey . '}');
  }
};

sub qpop($$) {
    my ($self, $result) = @_;
    if (! defined $result) {
      $self->render_not_found;
    } else {
      #b.map! do |q| q = '/q/'+q end
      my $json = JSON->new->allow_nonref;
      #$self->render(text => $json->pretty->encode($result));
      $redis->execute("get" => [$result] => sub{
                                               my ($redis, $res) = @_;
                                               my $value = $res->[0];
                                               if (! defined $res) {
                                                 $self->render(text => 'empty value');
                                               } else {
                                                 #b.map! do |q| q = '/q/'+q end
                                                 my $json = JSON->new->allow_nonref;
                                                 $self->render(text => $json->pretty->encode($value));
                                                 #throw :halt, [200, "{'value':" + v + "'key':" + b + "}"] unless v == nil 
                                               }
                                               });
    }
};

get '/q/:queue' => sub {
  my $self = shift;
  my $result = undef;
  my $queue  = $self->param('queue');
  my $soft = $self->param('soft');
  if (! defined $queue ) {
    $self->app->log->debug('queue was not in the URL ('.$queue.')');
    $self->render_not_found;
  } else {
    $queue .= $QUEUE_SUFFIX;
    if (defined $soft) {
      $redis->execute("lindex" => [$queue, -1] => sub{
                                           my ($redis, $res) = @_; 
                                           my $key = $res->[0]; 
                                           qpop($self, $key);
                                                     });
    } else {
      $redis->execute("rpop" => [$queue] => sub{
                                           my ($redis, $res) = @_; 
                                           my $key = $res->[0]; 
                                           qpop($self, $key);
                                               });
    }
  }
};

app->log->level('debug');
app->start;
__DATA__

@@ not_found.html.ep
% layout 'default';
% title 'Not Found';
Queue is empty

@@ index.html.ep
% layout 'default';
% title 'Welcome';
Welcome to Mojolicious!

@@ layouts/default.html.ep
<!doctype html><html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>

__END__

