#!/usr/bin/env perl
# Mojolicious minimalist RestMQ
# no COMET, just /q/ routes and queue logic
# the core of RestMQ is how it uses Redis' data types

# This app is diverging from the first demo in that it uses
# a different version of redis libs: AnyEvent::Redis. The
# interface seems a little cleaner and the callback functions
# as I would expect. This is not an implementation of the whole
# restmq as implemented by the python project but the sanatra
# gist.

use EV;
use JSON;
use AnyEvent;
use AnyEvent::Redis;
use Mojolicious::Lite;


my $QUEUESET     = 'QUEUESET';   # queue index
my $UUID_SUFFIX  = ':UUID';      # queue unique id
my $QUEUE_SUFFIX = ':queue';     # suffix to identify each queue's LIST

my $redis = AnyEvent::Redis->new(
      host => '127.0.0.1',
      port => 6379,
      encoding => 'utf8',
      on_error => sub { warn @_ },
  );

sub qpop($$) {
    my ($self, $result) = @_;
    if (! defined $result) {
      $self->render_not_found;
    } else {
      my $value = undef;
      #b.map! do |q| q = '/q/'+q end
      my $json = JSON->new->allow_nonref;
      #$self->render(text => $json->pretty->encode($result));
      $redis->all_cv(undef);
      $redis->all_cv->begin;
      $redis->get($result, sub{$value=shift;$redis->all_cv->end;});
      $redis->all_cv->recv;
      if (! defined $value) {
        $self->render(text => 'empty value');
      } else {
        #b.map! do |q| q = '/q/'+q end
        my $json = JSON->new->allow_nonref;
        $self->render(text => $json->pretty->encode($value));
        #throw :halt, [200, "{'value':" + v + "'key':" + b + "}"] unless v == nil 
      }
    }
};

get '/q' => sub {
  my $self = shift;
  my $result = undef;
  $redis->all_cv(undef);
  $redis->all_cv->begin;
  $redis->smembers($QUEUESET, sub{$result=shift;$redis->all_cv->end;});
  $redis->all_cv->recv;
  if (! defined $result || $result == 0) {
    $self->render_not_found;
  } else {
    #b.map! do |q| q = '/q/'+q end
    my $json = JSON->new->allow_nonref;
    $self->render(text => $json->pretty->encode($result))
  }
};

post '/q/:queue' => sub {
  my $self = shift;
  my $result = undef;
  my $queue  = $self->param('queue');
  my $value = $self->param('value');
  if (! defined $queue) {
    $self->app->log->debug('queue was not in the URL ('.$queue.')');
    $self->render_not_found;
  } else {
    my $uuid = undef;
    my $lkey = undef;
    my $q1   = $queue . $QUEUE_SUFFIX;
    my $q2   = $queue . $UUID_SUFFIX;

    $redis->all_cv(undef);
    $redis->all_cv->begin;
    $redis->incr($q2, sub{$uuid=shift;$redis->all_cv->end;});
    $redis->all_cv->recv;

    $self->app->log->debug('the uuid is ('.($uuid||'undefined').')');
    $lkey = $queue . ':' . $uuid;
    $redis->sadd($QUEUESET, $q1);
    $redis->set($lkey, $value);
    $redis->lpush($q1, $lkey);
    $self->app->log->debug('the uuid q is ('.($q2||'undefined').')');
    $self->render(text => '{ok, ' . $lkey . '}');
  }
};

get '/q/:queue' => sub {
  my $self = shift;
  my $result = undef;
  my $queue  = $self->param('queue');
  my $soft = $self->param('soft');
  if (! defined $queue ) {
    $self->app->log->debug('queue was not in the URL ('.$queue.')');
    $self->render_not_found;
  } else {
    my $key = undef;
    $queue .= $QUEUE_SUFFIX;
    if (defined $soft) {
      $redis->all_cv(undef);
      $redis->all_cv->begin;
      $redis->lindex($queue, -1, sub{$key=shift;$redis->all_cv->end;});
      $redis->all_cv->recv;
      qpop($self, $key);
    } else {
      $redis->all_cv(undef);
      $redis->all_cv->begin;
      $redis->rpop($queue, sub{$key=shift;$redis->all_cv->end;});
      $redis->all_cv->recv;
      qpop($self, $key);
    }
  }
};

app->log->level('debug');
app->start;
__DATA__

@@ not_found.html.ep
% layout 'default';
% title 'Not Found';
Queue is empty

@@ index.html.ep
% layout 'default';
% title 'Welcome';
Welcome to Mojolicious!

@@ layouts/default.html.ep
<!doctype html><html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>

__END__

