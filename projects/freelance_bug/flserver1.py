
import sys
import zmq

endpoints = ['tcp://127.0.0.1:5555',]

ctx = zmq.Context()
socket = zmq.Socket(ctx, zmq.PULL)
for e in endpoints:
    print "binding to %s ..." % e
    socket.bind(e)
    print "bound to %s" % e

for a in xrange(1,10):
    print socket.recv()
