
import sys
import time

import zmq


endpoints = ['tcp://127.0.0.1:5555','tcp://127.0.0.1:5556']

ctx = zmq.Context()
socket = zmq.Socket(ctx,zmq.PUSH)
socket.setsockopt(zmq.LINGER, 1)
for e in endpoints:
    print "connecting to %s..." % e
    socket.setsockopt(zmq.HWM, 1)
    socket.connect(e)
    print "connectec to %s" % e


for a in xrange(1,10):
    print "sending message %d..." % a
    try:
        socket.send("here is message %d" % a, flags=zmq.NOBLOCK)
        print "sent message %d" % a
    except Exception, err:
        print err
        print "re-sending message %d..." % a
        try:
            socket.send("here is message %d" % a, flags=zmq.NOBLOCK)
            print "sent message %d" % a
        except Exception, err:
            print "failed", err
    time.sleep(1)

