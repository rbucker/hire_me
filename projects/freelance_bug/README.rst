This is a BUG in ZeroMQ or pyzmq. I'm using this simple repository in order
to interact with the devs and or document the issue more completely. 

BACKGROUND:
-----------

According to the ZMQ (http://www.zeromq.org) documentation they make the following claims:

from the reference manual (zmq_socket):
 With the exception of ZMQ_PAIR, ØMQ sockets may be connected to multiple endpoints using zmq_connect()

[on the same page referring to ZMQ_PUSH]
 Messages are round-robined to all connected downstream nodes.

[continuing on...]
When a ZMQ_PUSH socket enters an exceptional state due to having reached the high water mark for all downstream nodes, or if there are no downstream nodes at all, then any zmq_send(3) operations on the socket shall block until the exceptional state ends or at least one downstream node becomes available for sending; messages are not discarded.


PRODUCTION:
----------

this is an interesting issue for me because while the ZMQ team uses terms like ventilator ... when the ventilator is running on a webserver there is one ventilator per thread. And so a many:many pattern is required... and the webserver needs to initiate them.


CONTRAST:
--------

flclient1.py suggests that the client code has the time to test each of the endpoints for their availability. While it is true it is contrary to the notion promoted in the guide.


STATUS:
------
"min" posted a link to this text: [ØMQ connections] exist when a client does zmq_connect(3) to an endpoint, whether or not a server has already done zmq_bind(3) to that endpoint.

From my position this is a crappy excuse. It shifts some of the responsibility around and that's it. But in my case it's going to turn out to be a lot less code. Still on the fense.

I currently believe that this is either a bug or overstatement in the guide or a bug in either pyzmq or zmq. I have looked at both source trees and I have not been able to identify the code for round-robin. The next step is a download and grep but for the moment I'm hoping this adequately demonstrates the issue at hand.


__END__
