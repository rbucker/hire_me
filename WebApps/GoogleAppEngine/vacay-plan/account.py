#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import os
import logging
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
import vacay_config, utils


class MainHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account.html',vals))

class RegisterHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_register.html',vals))

class JoinHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_join.html',vals))

class RequestHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_request.html',vals))

class ResponseHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_response.html',vals))

class ReportsHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_reports.html',vals))

class TemplatesHandler(webapp.RequestHandler):

	def get(self):
		vals = {'account_active':'active'}
		self.response.out.write(utils.render('account_templates.html',vals))


def main():
	application = webapp.WSGIApplication([
											('/account/register/.*', RegisterHandler),
											('/account/join/.*', JoinHandler),
											('/account/request/.*', RequestHandler),
											('/account/response/.*', ResponseHandler),
											('/account/reports/.*', ReportsHandler),
											('/account/templates/.*', TemplatesHandler),
											('/account/.*', MainHandler),
										 ], debug=True)
	util.run_wsgi_app(application)


if __name__ == '__main__':
	main()
