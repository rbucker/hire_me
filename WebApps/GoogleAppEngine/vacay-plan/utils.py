from google.appengine.ext.webapp import template

import os
import vacay_config

def render(doc,vals):
	vals['content'] = render_section(doc,vals)
	vals['sidebar'] = render_section('sidebar.html',vals)
	return render_section('index.html',vals)
    
def render_section(doc,vals):
	template_values = vacay_config.template_values()
	template_values.update(vals)
	path = os.path.join(os.path.dirname(__file__),'templates', doc)
	return template.render(path, template_values)



# __END__