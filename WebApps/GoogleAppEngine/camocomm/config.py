import datetime

def template_values():
	retval = {
			'title':'Vacation Planner',
			'base_url':'http://www.vacayplanner.com',
			'client_name':'FL2IT',
			'':'',
			'copyright_years':'2010',
			'copyright':'Florida Freelance IT LLC',
			'copyright_url':'http://www.fl2it.com',
			'now':datetime.datetime.now().strftime("%A %d. %B %Y at %I:%M %p"),
			'noreply_email':'noreply@vacayplanner.com',
			'admin_email':'richard@vacayplanner.com',
			'contact_email':'richard@vacayplanner.com',
			'general_email':'richard@vacayplanner.com',
			'sales_email':'richard@vacayplanner.com',
			'tech_email':'richard@vacayplanner.com',
			'keywords':'vacation planner vacay plan',
			'description':'free soho enterprise vacation planner',
			}
	return retval