#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import os, logging, email, re
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler 
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api.labs import taskqueue
from google.appengine.api import mail


import alerts_global, models

class LogSenderHandler(InboundMailHandler):
    def receive(self, mail_message):
        template_values = alerts_global.template_values()
        m = re.search('\[([^\[\]]*)\]', mail_message.subject)
        park = m.group(1).upper()
        m = re.search('\<([^\[\]]*)\>', mail_message.sender)
        sender = m.group(1)
        if len(sender) == 0:
            sender = mail_message.sender
        logging.info("Received a message from: %s for %s" % (sender,park))
        
        pks = models.lookupPrimary(sender,park)
        if pks == None:
            logging.info("could not match the primary")
            pks = models.lookupAlternate(sender,park)
            if pks == None:
                logging.info("could not match the alternate")
                # just ignore it
                return
                
        # save the request
        alert = models.createAlert(pks,mail_message.subject)
        alert.put()
        # queue the request
        t = taskqueue.Task(url='/tasks/alert/', method='GET',params={'alert': str(alert.key())})
        t.add('alert-queue')

        # send a reply
        body = ''' We received your message "%s" and it has been queued for delivery.
        
        Thanks!
        Park Alerts
        '''
        mail.send_mail(template_values['noreply_email'], mail_message.sender, 'Alert Received', body % (mail_message.subject))
        logging.info("Queued message from: %s for %s" % (sender,park))

def main():
    application = webapp.WSGIApplication([LogSenderHandler.mapping()], debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

#__END__