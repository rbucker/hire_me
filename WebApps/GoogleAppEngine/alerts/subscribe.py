#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import os
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.api import mail
import alerts_global 
import models

#import admin

def subscribeContent(template_values):
    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
    postpath = os.path.join(os.path.dirname(__file__),'templates', template_values['content_snippet'])
    sidebarpath = os.path.join(os.path.dirname(__file__),'templates', 'sidebar.html')
    template_values['content'] = template.render(postpath, template_values)
    template_values['sidebar'] = template.render(sidebarpath, template_values)
    template_values['focus'] = 'document.forms[0].park_select.focus()'
    return template.render(indexpath, template_values)
 
class MainHandler(webapp.RequestHandler):

    def get(self):
        template_values = alerts_global.template_values()
        template_values['content_snippet'] = 'subscribe.html'
        
        ack = self.request.get('ack')
        if len(ack)>0:
            if models.activateSubscriber(ack):
                template_values['content_snippet'] = 'subscribe_conf_thankyou.html'
            else:
                template_values['content_snippet'] = 'invalid_ack.html'
        else:
            template_values['park_list'] = models.park_options(None)
            
        self.response.out.write(subscribeContent(template_values))

    def post(self):
        park_select = self.request.get("park_select")
        email = self.request.get("email")
        
        template_values = alerts_global.template_values()
        template_values['park_select'] = park_select
        template_values['email'] = email
        
        # validate the request
        err_count = 0
        if (len(park_select)<=0):
            template_values['park_select_err'] = 'Field is required'
            err_count = err_count +1
        if (len(email)<=4):
            template_values['email_err'] = 'Field is required'
            err_count = err_count +1
        if (models.validateEmail(email)==0):
            template_values['email_err'] = 'Invalid email address'
            err_count = err_count +1
        if (err_count > 0):
            self.response.out.write(registerForm(template_values))
            return

        alt = models.create_subscriber(template_values)
        pks = models.PrimaryAuthority.get(template_values['park_select'])
        template_values['park_name'] = pks.park_name
        template_values['link'] = self.request.url+'?ack='+str(alt.key())+'&email='+email
    
        confpath = os.path.join(os.path.dirname(__file__),'templates', 'subscribe_conf.email')
        template_values['park_list'] = models.park_options(None)
        body = template.render(confpath, template_values)
        #email_to get the email address from the DB
        mail.send_mail(template_values['noreply_email'], email, 'Subscription Confirmation', body)

        indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
        sidebarpath = os.path.join(os.path.dirname(__file__),'templates', 'sidebar.html')
        sidebar = template.render(sidebarpath, template_values)
        postpath = os.path.join(os.path.dirname(__file__),'templates', 'subscribe_ack.html')
        content = template.render(postpath, template_values)
        template_values['content'] = content
        template_values['sidebar'] = sidebar
        
        self.response.out.write(template.render(indexpath, template_values))



def main():
    application = webapp.WSGIApplication([
                                       ('/subscribe/', MainHandler),
                                       ],
                                       debug=True)
    
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
