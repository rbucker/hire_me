from types import *
import os
import logging
#from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import config
#from models import User, Partner

logger = logging.getLogger()


#def authorize(mode):
#    if mode != 'home':
#        user = users.get_current_user()
#        if user:
#            user_role = getRole(user)        
#            if (mode == 'root' and user_role == 'root') or (mode == 'admin' and user_role == 'admin') or mode == 'user':
#                return user_role
#        else:
#            raise NotLoggedInError
#    return True

def render(menu_mode, content):
    return render_with_data(menu_mode, content,[])
    
def render_with_data(menu_mode, content,data):
    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
    menu_mode=''
    user_mode=''

    #if menu_mode == 'home':
    #    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index_home.html')
    template_values = config.template_values()
    template_values[menu_mode+'_current'] = 'current'
    template_values['data'] = data
    if type(data) is DictionaryType:
        template_values.update(data)
    template_values['main_menu'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'menu'+user_mode+'.html'), 
        template_values)
    template_values['about'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'about'+user_mode+'.html'), 
        template_values)
    template_values['sidebar_menu'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'sidebar'+menu_mode+'.html'), 
        template_values)
    template_values['content'] = template.render(os.path.join(os.path.dirname(__file__),'templates', content), 
        template_values)
    template_values['contact'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'contact.html'), 
        template_values)
    template_values['footer'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'footer.html'), 
        template_values)
    retval = template.render(indexpath, template_values)
    return retval

#__END__