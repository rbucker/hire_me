#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import logging
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.api import mail
import alerts_global 
import models

#import admin

#
# QUEUES: (registration_email, confirmation_timeout)
# MODEL: (Authority)
#
# 1) prompt the user with the registration page
# 2) record the post with confirmed = NO
# 3) queue an email for confirmation
# 4) process the queue event; send the email
# 5) queue an expiration event
# 6) if the expiration event occurs if confirmation=NO then remove
#



def registerContent(template_values):
    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
    postpath = os.path.join(os.path.dirname(__file__),'templates', template_values['content_snippet'])
    sidebarpath = os.path.join(os.path.dirname(__file__),'templates', 'sidebar.html')
    template_values['content'] = template.render(postpath, template_values)
    template_values['sidebar'] = template.render(sidebarpath, template_values)
    template_values['focus'] = 'document.forms[0].official.focus()'
    return template.render(indexpath, template_values)
    
class MainHandler(webapp.RequestHandler):
    logging.getLogger().setLevel(logging.DEBUG)

    def get(self):
        template_values = alerts_global.template_values()
        template_values['content_snippet'] = 'register.html'
        
        ack = self.request.get('ack')
        if len(ack)>0:
            if models.activatePrimary(ack):
                template_values['content_snippet'] = 'subscribe_conf_thankyou.html'
            else:
                if models.activateAlternate(ack):
                    template_values['content_snippet'] = 'subscribe_conf_thankyou.html'
                else:
                    template_values['content_snippet'] = 'invalid_ack.html'
        else:
            template_values['park_list'] = models.park_options(None)
        self.response.out.write(registerContent(template_values))

    def post(self):
        official = self.request.get("official")
        city = self.request.get("city")
        state = self.request.get("state")
        country = self.request.get("country")
        park = self.request.get("park").upper()
        park_select = self.request.get("park_select")
        email = self.request.get("email")
        park_name = park+', '+city+' '+state+' '+country
        
        template_values = alerts_global.template_values()
        template_values['city'] = city
        template_values['state'] = state
        template_values['country'] = country
        template_values['park'] = park
        template_values['park_select'] = park_select
        template_values['email'] = email
        template_values['park_name'] = park_name
        template_values[official+'_official'] = 'selected'
        
        # validate the request
        err_count = 0
        if (len(email)<=4):
            template_values['email_err'] = 'Field is required'
            err_count = err_count +1
        if (models.validateEmail(email)==0):
            template_values['email_err'] = 'Invalid email address'
            err_count = err_count +1
        if (official == 'primary'):
            if (len(city)<=0):
                template_values['city_err'] = 'Field is required'
                err_count = err_count +1
            if (len(state)<=0):
                template_values['state_err'] = 'Field is required'
                err_count = err_count +1
            if (len(country)<=0):
                template_values['country_err'] = 'Field is required'
                err_count = err_count +1
            if (len(park)<=0):
                template_values['park_err'] = 'Field is required'
                err_count = err_count +1
            # does the park exist already
            if models.park_exists(park_name):
                template_values['park_err'] = 'Duplicate Park'
                err_count = err_count +1
        else:
            if (len(park_select)<=0):
                template_values['park_select_err'] = 'Field is required'
                err_count = err_count +1
            
        if (err_count > 0):
            self.response.out.write(registerForm(template_values))
            return

        # save the request
        if (official == 'primary'):
            pks = models.create_primary(template_values)
            template_values['link'] = self.request.url+'?ack='+str(pks.key())+'&email='+email
        else:
            alt = models.create_alternate(template_values)
            pks = models.PrimaryAuthority.get(template_values['park_select'])
            template_values['park_name'] = pks.park_name
            template_values['link'] = self.request.url+'?ack='+str(alt.key())+'&email='+email
        
        # email the request
        if (official == 'primary'):
            confpath = os.path.join(os.path.dirname(__file__),'templates', 'register_conf.email')
            body = template.render(confpath, template_values)
            mail.send_mail(template_values['noreply_email'], email, 'Registration Confirmation', body)
        else:
            confpath = os.path.join(os.path.dirname(__file__),'templates', 'register_alt_conf.email')
            template_values['park_list'] = models.park_options(None)
            body = template.render(confpath, template_values)
            #email_to get the email address from the DB
            mail.send_mail(template_values['noreply_email'], pks.email, 'Registration Confirmation', body)

        indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
        sidebarpath = os.path.join(os.path.dirname(__file__),'templates', 'sidebar.html')
        sidebar = template.render(sidebarpath, template_values)
        postpath = os.path.join(os.path.dirname(__file__),'templates', 'register_ack.html')
        if (official == 'alternate'):
            postpath = os.path.join(os.path.dirname(__file__),'templates', 'register_alt_ack.html')
        content = template.render(postpath, template_values)
        template_values['content'] = content
        template_values['sidebar'] = sidebar
        
        self.response.out.write(template.render(indexpath, template_values))


def main():
    application = webapp.WSGIApplication([
                                   ('/register/', MainHandler),
                                   ],
                                   debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

# __END__