import datetime

def template_values():
    retval = {
            'title':'Park Alerts',
            'base_url':'http://www.parkalerts.info',
            'client_name':'FL2IT',
            '':'',
            'copyright_years':'2010',
            'copyright':'Florida Freelance IT LLC',
            'copyright_url':'http://www.fl2it.com',
            'now':datetime.datetime.now().strftime("%A %d. %B %Y at %I:%M %p"),
            'noreply_email':'noreply@parkalerts.info',
            'admin_email':'richard@parkalerts.info',
            }
    return retval