import logging, datetime
from google.appengine.ext import db

# I have omitted dates and times.... while they are interesting for reports
# and such things.  I don't care about that right now so why bother tracking
# all that junk.
class PrimaryAuthority(db.Model):
    park_name = db.StringProperty(required=True)
    country = db.StringProperty(required=True)
    state = db.StringProperty(required=True)
    city = db.StringProperty(required=True)
    park = db.StringProperty(required=True)
    email = db.StringProperty(required=True)
    active = db.BooleanProperty()

class AlternateAuthority(db.Model):
    primary_authority = db.ReferenceProperty(PrimaryAuthority)
    email = db.StringProperty(required=True)
    active = db.BooleanProperty()

class Subscriber(db.Model):
    primary_authority = db.ReferenceProperty(PrimaryAuthority)
    email = db.StringProperty(required=True)
    active = db.BooleanProperty()

class Alert(db.Model):
    primary_authority = db.ReferenceProperty(PrimaryAuthority)
    message = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    

# need some sensible macros to reduce the overal amount of code and
# basic complexity... and try for "transaction seeparation"
def park_by_park_name(park_name):
    return db.GqlQuery("SELECT * FROM PrimaryAuthority WHERE park_name = :1",park_name)

def park_by_key(park_key):
    return PrimaryAuthority.get(db.Key(park_key))

def park_exists(park_name):
    pks = park_by_park_name(park_name)
    if pks.count() == 0:
        return None
    return True
    
def create_primary(v):
    park = PrimaryAuthority(
            park_name = v['park_name'],
            country = v['country'],
            state = v['state'],
            city = v['city'],
            park = v['park'],
            email = v['email'],
            active=False,
        )
    db.put(park)
    return park
    
def create_alternate(v):
    pks = PrimaryAuthority.get(v['park_select'])
    # check if altname and primary already exists
    if pks != None:
        # do not create alt if it already exists
        alt = db.GqlQuery("SELECT * FROM AlternateAuthority WHERE email = :1 and primary_authority = :2",v['email'], db.Key(v['park_select']))
        # if there is more than one... that would really bad
        if alt.count() == 1:
            return alt.get()
        alt = AlternateAuthority(
                primary_authority = pks.key(),
                email = v['email'],
                active=False,
                )
        db.put(alt)
        return alt
    return None
    
def create_subscriber(v):
    pks = PrimaryAuthority.get(v['park_select'])
    # check if altname and primary already exists
    if pks != None:
        # do not create alt if it already exists
        alt = db.GqlQuery("SELECT * FROM Subscriber WHERE email = :1 and primary_authority = :2",v['email'], db.Key(v['park_select']))
        # if there is more than one... that would really bad
        if alt.count() == 1:
            return alt.get()
        alt = Subscriber(
                primary_authority = pks.key(),
                email = v['email'],
                active=False,
                )
        db.put(alt)
        return alt
    return None

def createAlert(pks,message):
    retval = Alert(primary_authority=pks, message=message)
    return retval

def activateSubscriber(ack):
    try:
        pks = Subscriber.get(ack)
        pks.active=True
        pks.put()
    except db.BadKeyError:
        return None
    return 1

def activatePrimary(ack):
    try:
        pks = PrimaryAuthority.get(db.Key(ack))
        pks.active=True
        pks.put()
    except db.KindError:
        return None
    except db.BadKeyError:
        return None
    return 1

def activateAlternate(ack):
    try:
        pks = AlternateAuthority.get(db.Key(ack))
        pks.active=True
        pks.put()
    except db.KindError:
        return None
    except db.BadKeyError:
        return None
    return 1

def lookupPrimary(sender,park):
    logging.info("lookupPrimary (%s,%s)" % (sender,park))
    pks = db.GqlQuery("SELECT * FROM PrimaryAuthority WHERE email = :1 and park = :2 and active = :3",sender,park,True)
    if pks.count() == 1:
        pk = pks.get()
        if pk.active == True:
            return pk
    return None

def lookupAlternate(sender,park):
    logging.info("lookupAlternate (%s,%s)" % (sender,park))
    alt = db.GqlQuery("SELECT * FROM AlternateAuthority WHERE email = :1 and active = :2",sender,True)
    if alt.count() > 0:
        for a in alt:
            pks = db.GqlQuery("SELECT * FROM PrimaryAuthority WHERE __key__ = :1 and park = :2 and active = :3",a.primary_authority,park,True)
            if pks.count() == 1:
                pk = pks.get()
                if pk.active == True:
                    return pk
    return None

def alert_history(primary_authority):
    retval = ''
    alerts = db.GqlQuery("SELECT * FROM Alert WHERE primary_authority = :1 limit 30",db.Key(primary_authority))
    for a in alerts:
        dt = a.created
        retval = retval + "<tr><td>" + dt.strftime('%Y-%m-%d %I:%M %p UTC') + "</td><td>" + a.message + "</td></tr>\n"
    return retval
    
def park_options(selected):
    retval = ''
    parks = PrimaryAuthority.all()
    for p in parks:
        retval = retval + "<option value=\""+str(p.key())+"\">"+p.park_name+"</option>\n"
    return retval


    
def subscriberList(primary_authority):
    pks = db.GqlQuery("SELECT * FROM Subscriber WHERE primary_authority = :1 and active = :2",primary_authority,True)
    return pks

def validAuthority(auth,park):
    #    alt = db.GqlQuery("SELECT * FROM PrimaryAuthority WHERE email = :1 and primary_authority = :2",v['email'], db.Key(v['park_select']))
    return

import re
def validateEmail(email):
    if len(email) > 7:
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
			return 1
    return 0

# __END__