#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import datetime
import logging
from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError, TooManyReferences, PageNotFound
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.runtime import DeadlineExceededError

import config
#from ctool import *
import ctool
from models import Client, State, Store, Project, Shift, User, Partner, Rate, Employee, Assignment, Week, Timesheet, Expense

logger = logging.getLogger()

def deactivate(p):
    p.is_active = False
    p.put()
    
def activate(p):
    p.is_active = True
    p.put()
    
def releaseRecord(o):
    if o.refcount == 0:
        ctool.releaseReference(o.partner)
        o.delete()
    else:
        raise TooManyReferences

def getReference(p):
    return ctool.getReference(p)

class MainHandler(webapp.RequestHandler):
    def post(self,mode=''):
        try:
            user = ctool.authorize('admin')
            data = {}
            if mode == 'client':
                name = self.request.get('name')
                key = self.request.get('key')
                u = None
                if key:
                    u = Client.get(key)
                    u.name = name
                else:
                    u = Client(partner=ctool.getPartnerRef(),name=name,is_active=True)
                u.put()
            elif mode == 'manager':
                user = ctool.authorize('root')
                name = self.request.get('name')
                email = self.request.get('email')
                key = self.request.get('key')
                u = None
                if key:
                    u = User.get(key)
                    u.name = name
                    u.email = email
                else:
                    u = User(partner=ctool.getPartnerRef(),name=name,email=email,is_manager=True,is_active=True)
                u.put()
            elif mode == 'employee':
                lastname = self.request.get('lastname')
                firstname = self.request.get('firstname')
                mi = self.request.get('mi')
                rate_id = self.request.get('rate')
                rate = Rate.get(rate_id)
                perdiem = self.request.get('perdiem')
                addr1 = self.request.get('addr1')
                addr2 = self.request.get('addr2')
                city = self.request.get('city')
                state = self.request.get('state')
                zip = self.request.get('zip')
                phone1 = self.request.get('phone1')
                phone2 = self.request.get('phone2')
                nontravel_employee = self.request.get('nontravel_employee')
                buddy_id = self.request.get('buddy')
                buddy = None
                if buddy_id:
                    buddy = Employee.get(buddy_id)
                paystub_required = self.request.get('paystub_required')
                is_active = self.request.get('is_active')
                key = self.request.get('key')
                u = None
                if key:
                    u = Employee.get(key)
                    u.lastname = lastname
                    u.firstname = firstname
                    u.mi = mi
                    ctool.reReleaseReference(u.rate,rate)
                    u.rate = rate
                    u.perdiem = float(perdiem)
                    u.addr1 = addr1
                    u.addr2 = addr2
                    u.city = city
                    u.state = state
                    u.zip = zip
                    u.phone1 = phone1
                    u.phone2 = phone2
                    u.nontravel_employee = bool(nontravel_employee)
                    ctool.reReleaseReference(u.buddy,buddy)
                    u.buddy = buddy
                    u.paystub_required = bool(paystub_required)
                    u.is_active = bool(is_active)
                else:
                    u = Employee(partner=ctool.getPartnerRef(),lastname=lastname,firstname=firstname,
                            mi=mi,rate=rate,addr1=addr1,addr2=addr2,city=city,state=state,zip=zip,
                            phone1=phone1,phone2=phone2,nontravel_employee=bool(nontravel_employee),
                            buddy=buddy,paystub_required=bool(paystub_required),perdiem=float(perdiem),
                            is_active=bool(is_active))
                    ctool.getReference(rate)
                    ctool.getReference(buddy)
                u.put()
            elif mode == 'assignment':
                shift_id = self.request.get('this_shift')
                shift = Shift.get(shift_id)
                employee_id = self.request.get('employee')
                employee = Employee.get(employee_id)
                key = self.request.get('key')
                u = None
                if key:
                    u = Assignment.get(key)
                    ctool.reReleaseReference(u.shift,shift)
                    u.shift = shift
                    ctool.reReleaseReference(u.employee,employee)
                    u.employee = employee
                else:
                    u = Assignment(partner=ctool.getPartnerRef(),shift=shift,employee=employee)
                    ctool.getReference(u.shift)
                    ctool.getReference(u.employee)
                u.put()
                employee.shift = shift
                employee.put()
                return self.redirect("/admin/assignment?this_shift="+shift_id)
            elif mode == 'project':
                name = self.request.get('name')
                store_id = self.request.get('store')
                store = Store.get(store_id)
                key = self.request.get('key')
                u = None
                if key:
                    u = Project.get(key)
                    u.name = name
                    ctool.reReleaseReference(u.store,store)
                    u.store = store
                else:
                    u = Project(partner=ctool.getPartnerRef(),name=name,store=store)
                    ctool.getReference(u.store)
                u.put()
            elif mode == 'rate':
                name = self.request.get('name')
                amount = self.request.get('amount')
                key = self.request.get('key')
                u = None
                if key:
                    u = Rate.get(key)
                    u.name = name
                    u.amount = float(amount)
                else:
                    u = Rate(partner=ctool.getPartnerRef(),name=name,amount=float(amount),is_active=True)
                u.put()
            elif mode == 'shift':
                project_id = self.request.get('project')
                project = Project.get(project_id)
                name = self.request.get('name')
                key = self.request.get('key')
                u = None
                if key:
                    u = Shift.get(key)
                    u.name = name
                    ctool.reReleaseReference(u.project,project)
                    u.project = project
                else:
                    u = Shift(partner=ctool.getPartnerRef(),name=name,project=project)
                    ctool.getReference(project)
                u.put()
            elif mode == 'store':
                client_id = self.request.get('client')
                client = Client.get(client_id)
                state_id = self.request.get('state')
                state = State.get(state_id)
                manager_id = self.request.get('manager')
                manager = User.get(manager_id)
                name = self.request.get('name')
                key = self.request.get('key')
                u = None
                if key:
                    u = Store.get(key)
                    u.name = name
                    ctool.reReleaseReference(u.manager,manager)
                    u.manager = manager
                    ctool.reReleaseReference(u.client,client)
                    u.client = client
                    ctool.reReleaseReference(u.state,state)
                    u.state = state
                else:
                    u = Store(partner=ctool.getPartnerRef(),manager=manager,name=name,client=client,state=state)
                    ctool.getReference(u.manager)
                    ctool.getReference(u.client)
                    ctool.getReference(u.state)
                u.put()
            elif mode == 'state':
                name = self.request.get('name')
                key = self.request.get('key')
                u = None
                if key:
                    u = State.get(key)
                    u.name = name
                else:
                    u = State(partner=ctool.getPartnerRef(),name=name,is_active=True)
                u.put()
            elif mode == 'week':
                tmp = self.request.get('week_ending')
                week_ending = datetime.datetime.strptime(tmp,"%Y-%m-%d").date()
                key = self.request.get('key')
                u = None
                if key:
                    u = Week.get(key)
                    u.week_ending = week_ending
                else:
                    u = Week(partner=ctool.getPartnerRef(),week_ending=week_ending,is_active=True)
                u.put()
            elif mode == 'reports':
                pass
            elif mode == 'alerts':
                pass
            elif mode == 'timesheet':
                pass
            elif mode == 'expense':
                shift_id = self.request.get('this_shift')
                shift = Shift.get(shift_id)
                week_id = self.request.get('this_week')
                week = Week.get(week_id)
                description = self.request.get('description')
                amount = self.request.get('amount')
                key = self.request.get('key')
                u = None
                if key:
                    u = Expense.get(key)
                    ctool.reReleaseReference(u.shift,shift)
                    u.shift = shift
                    ctool.reReleaseReference(u.week_ending,week)
                    u.week_ending = week
                    u.description = description
                    u.amount = float(amount)
                else:
                    u = Expense(partner=ctool.getPartnerRef(),shift=shift,week_ending=week,description=description,amount=float(amount))
                    ctool.getReference(u.shift)
                    ctool.getReference(u.week_ending)
                u.put()
                return self.redirect("/admin/expense?this_shift="+shift_id+"&this_week="+week_id)
            else:
                raise PageNotFound
            self.redirect('/admin/'+mode)
        except db.BadValueError:
            self.redirect('/error/content_badvalue.html')
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except PageNotFound:
            self.redirect('/error/content_pagenotfound.html')
    def get(self,mode='admin'):
        try:
            user = ctool.authorize('root')
            data = {}
            if mode == 'client':
                p = Client.all().filter("partner =",ctool.getPartner()).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Client.get(key)
                data = {'clients':p,'i':i}
            elif mode == 'toggle_client':
                user = ctool.authorize('admin')
                key = self.request.get('key')
                p = Client.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/client')
            elif mode == 'delete_client':
                key = self.request.get('key')
                i = None
                if key:
                    i = Client.get(key)
                    releaseRecord(i)
                return self.redirect('/admin/client')
            elif mode == 'manager':
                u = User.all().filter("partner = ",ctool.getPartner()).filter('is_manager =',True).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = User.get(key)
                data = {'users':u,'i':i}
            elif mode == 'toggle_manager':
                key = self.request.get('key')
                p = User.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/manager')
            elif mode == 'delete_manager':
                key = self.request.get('key')
                i = None
                if key:
                    i = User.get(key)
                    releaseRecord(i)
                return self.redirect('/admin/manager')
            elif mode == 'employee':
                employees = Employee.all().filter("partner = ",ctool.getPartner()).order('lastname').order('firstname')
                buddies = Employee.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('lastname').order('firstname')
                rates = Rate.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Employee.get(key)
                data = {'buddies':buddies,'employees':employees,'rates':rates,'i':i}
            elif mode == 'toggle_employee':
                key = self.request.get('key')
                p = Employee.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/employee')
            elif mode == 'delete_employee':
                key = self.request.get('key')
                i = None
                if key:
                    i = Employee.get(key)
                    ctool.releaseReference(i.rate)
                    releaseRecord(i)
                return self.redirect('/admin/employee')
            elif mode == 'assignment':
                shifts = Shift.all().filter("partner = ",ctool.getPartner()).order('name')
                employees = Employee.all().filter("partner = ",ctool.getPartner()).filter("shift = ",None).order('lastname').order('firstname')
                this_shift_id = self.request.get('this_shift')
                assignments = None
                this_shift = None
                if this_shift_id:
                    this_shift = Shift.get(this_shift_id)
                    assignments = Assignment.all().filter("partner = ",ctool.getPartner()).filter("shift =",this_shift)
                key = self.request.get('key')
                i = None
                if key:
                    i = Assignment.get(key)
                data = {'shifts':shifts,'employees':employees,'assignments':assignments,'this_shift':this_shift,'i':i}
            elif mode == 'toggle_assignment':
                key = self.request.get('key')
                shift_id = self.request.get('this_shift')
                p = Assignment.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect("/admin/assignment?this_shift="+shift_id)
            elif mode == 'delete_assignment':
                key = self.request.get('key')
                shift_id = self.request.get('this_shift')
                i = None
                if key:
                    i = Assignment.get(key)
                    i.employee.shift = None
                    i.put()
                    ctool.releaseReference(i.shift)
                    ctool.releaseReference(i.employee)
                    releaseRecord(i)
                return self.redirect("/admin/assignment?this_shift="+shift_id)
            elif mode == 'project':
                stores = Store.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('name')
                projects = Project.all().filter("partner = ",ctool.getPartner()).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Project.get(key)
                data = {'projects':projects,'stores':stores,'i':i}
            elif mode == 'toggle_project':
                key = self.request.get('key')
                p = Project.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/project')
            elif mode == 'delete_project':
                key = self.request.get('key')
                i = None
                if key:
                    i = Project.get(key)
                    ctool.releaseReference(i.store)
                    releaseRecord(i)
                return self.redirect('/admin/project')
            elif mode == 'rate':
                p = Rate.all().filter("partner =",ctool.getPartner()).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Rate.get(key)
                data = {'rates':p,'i':i}
            elif mode == 'toggle_rate':
                key = self.request.get('key')
                p = Rate.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/rate')
            elif mode == 'delete_rate':
                key = self.request.get('key')
                i = None
                if key:
                    i = Rate.get(key)
                    releaseRecord(i)
                return self.redirect('/admin/rate')
            elif mode == 'shift':
                shifts = Shift.all().filter("partner = ",ctool.getPartner()).order('name')
                projects = Project.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Shift.get(key)
                data = {'projects':projects,'shifts':shifts,'i':i}
            elif mode == 'toggle_shift':
                key = self.request.get('key')
                p = Shift.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/shift')
            elif mode == 'delete_shift':
                key = self.request.get('key')
                i = None
                if key:
                    i = Shift.get(key)
                    ctool.releaseReference(i.project)
                    releaseRecord(i)
                return self.redirect('/admin/shift')
            elif mode == 'store':
                clients = Client.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('name')
                states = State.all().filter("partner = ",ctool.getPartner()).filter('is_active =',True).order('name')
                managers = User.all().filter("partner = ",ctool.getPartner()).filter('is_manager =',True).filter('is_active =',True).order('name')
                stores = Store.all().filter("partner = ",ctool.getPartner()).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Store.get(key)
                data = {'states':states,'clients':clients,'stores':stores,'managers':managers,'i':i}
            elif mode == 'toggle_store':
                key = self.request.get('key')
                p = Store.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/store')
            elif mode == 'delete_store':
                key = self.request.get('key')
                i = None
                if key:
                    i = Store.get(key)
                    ctool.releaseReference(i.client)
                    ctool.releaseReference(i.state)
                    ctool.releaseReference(i.manager)
                    releaseRecord(i)
                return self.redirect('/admin/store')
            elif mode == 'state':
                p = State.all().filter("partner =",ctool.getPartner()).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = State.get(key)
                data = {'states':p,'i':i}
            elif mode == 'toggle_state':
                key = self.request.get('key')
                p = State.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/state')
            elif mode == 'delete_state':
                key = self.request.get('key')
                i = None
                if key:
                    i = State.get(key)
                    releaseRecord(i)
                return self.redirect('/admin/state')
            elif mode == 'week':
                p = Week.all().filter("partner =",ctool.getPartner()).order('week_ending')
                key = self.request.get('key')
                i = None
                if key:
                    i = Week.get(key)
                data = {'weeks':p,'i':i}
            elif mode == 'toggle_week':
                key = self.request.get('key')
                p = Week.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/admin/week')
            elif mode == 'delete_week':
                key = self.request.get('key')
                i = None
                if key:
                    i = Week.get(key)
                    releaseRecord(i)
                return self.redirect('/admin/week')
            elif mode == 'reports':
                pass
            elif mode == 'alerts':
                pass
            elif mode=='timesheet':
                pass
            elif mode=='lock_expense':
                shift_id = self.request.get('this_shift')
                week_id = self.request.get('this_week')
                this_shift = None
                if shift_id:
                    this_shift = Shift.get(shift_id)
                this_week = None
                if week_id:
                    this_week = Week.get(week_id)
                if this_shift and this_week:
                    expenses = Expense.all().filter("partner = ",ctool.getPartner()).filter("shift = ",this_shift).filter("week_ending = ",this_week).filter("is_active = ",True).filter("is_locked = ",False)
                    if expenses and expenses.count() > 0:
                        for e in expenses:
                            e.is_locked = True
                            e.put()
                return self.redirect('/admin/expense?this_shift='+shift_id+'&this_week='+week_id)
            elif mode=='unlock_expense':
                shift_id = self.request.get('this_shift')
                week_id = self.request.get('this_week')
                this_shift = None
                if shift_id:
                    this_shift = Shift.get(shift_id)
                this_week = None
                if week_id:
                    this_week = Week.get(week_id)
                if this_shift and this_week:
                    expenses = Expense.all().filter("partner = ",ctool.getPartner()).filter("shift = ",this_shift).filter("week_ending = ",this_week).filter("is_active = ",True).filter("is_locked = ",True)
                    if expenses and expenses.count() > 0:
                        for e in expenses:
                            e.is_locked = False
                            e.put()
                return self.redirect('/admin/expense?this_shift='+shift_id+'&this_week='+week_id)
            elif mode=='delete_expense':
                key = self.request.get('key')
                shift_id = self.request.get('this_shift')
                week_id = self.request.get('this_week')
                i = None
                if key:
                    i = Expense.get(key)
                    ctool.releaseReference(i.shift)
                    ctool.releaseReference(i.week_ending)
                    releaseRecord(i)
                return self.redirect('/admin/expense?this_shift='+shift_id+'&this_week='+week_id)
            elif mode=='expense':
                shifts = Shift.all().filter("partner = ",ctool.getPartner()).filter("is_active = ",True).order('name')
                weeks = Week.all().filter("partner = ",ctool.getPartner()).filter("is_active = ",True).order('week_ending')
                this_shift_id = self.request.get('this_shift')
                this_week_id = self.request.get('this_week')
                expenses = None
                is_locked = False
                this_shift = None
                if this_shift_id:
                    this_shift = Shift.get(this_shift_id)
                this_week = None
                if this_week_id:
                    this_week = Week.get(this_week_id)
                if this_shift_id and this_week_id:
                    expenses = Expense.all().filter("partner = ",ctool.getPartner()).filter("shift = ",this_shift).filter("week_ending = ",this_week).filter("is_active = ",True)
                    if expenses and expenses.count() > 0:
                        is_locked = expenses[0].is_locked
                key = self.request.get('key')
                i = None
                if key and not is_locked:
                    i = Expense.get(key)
                data = {'expenses':expenses,'shifts':shifts,'weeks':weeks,'this_shift':this_shift,'this_week':this_week,'i':i,'role':'admin','is_locked':is_locked}
            elif mode=='admin':
                pass
            elif mode=='todo':
                pass
            else:
                raise PageNotFound
            self.render_with_data('content_'+mode+'.html',data)
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except TooManyReferences:
            self.redirect('/error/content_refcount.html')
        except PageNotFound:
            self.redirect('/error/content_pagenotfound.html')
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
    def render(self,content):
        self.render_with_data(content,[])
    def render_with_data(self,content,data):
        try:
            user = ctool.authorize('admin')
            self.response.out.write(ctool.render_with_data('admin',content,data))
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except DeadlineExceededError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("This operation could not be completed in time...")
        except CapabilityDisabledError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("System appears to be be in database maintenance mode...")



def main():
    logger.setLevel(logging.DEBUG)
    
    application = webapp.WSGIApplication([
                                        ('/admin', MainHandler),
                                        ('/admin/(.*)', MainHandler),
                                        ],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
