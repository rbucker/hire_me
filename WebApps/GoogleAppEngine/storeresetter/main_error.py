#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import logging
from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.runtime import DeadlineExceededError

import config
import ctool


class MainHandler(webapp.RequestHandler):
    def get(self,content):
        self.render(content)
    def render(self,content):
        self.render_with_data(content,[])
    def render_with_data(self,content,data):
        try:
            #user = ctool.authorize('home')
            self.response.out.write(ctool.render_with_data('error',content,data))
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except DeadlineExceededError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("This operation could not be completed in time...")
        except CapabilityDisabledError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("System appears to be be in database maintenance mode...")

def main():
    logging.getLogger().setLevel(logging.DEBUG)
    application = webapp.WSGIApplication([
                                          ('/error', MainHandler),
                                          ('/error/(.*)', MainHandler),],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
