import datetime
import ctool
from google.appengine.api import users

def template_values():
    now = datetime.datetime.now()
    retval = {
            'title': ctool.getPartnerName(),
            'slogan':'just another way to reset...',
            'base_url':'http://www.storeresetter.com',
            'client_name':'FLFREEIT',
            'phone_number':'123-123-1233',
            'tollfree_number':'123-123-1233',
            'fax_number':'123-123-1233',
            'address':'3395 White Sulfer Road<br>Gainesville, GA 60060',
            'email_address':'richard@bucker.net',
            'home':'http://www.flfreeit.com',
            
            '':'',
            'copyright':'Florida Freelance IT LLC',
            'copyright_url':'http://www.flfreeit.com',
            'noreply_email':'noreply@storeresetter.com',
            'admin_email':'richard@storeresetter.com',

            'description':'Tab Retail Remodeling Inc. database workflow store resetter',
            'keywords':'Tab Retail Remodeling Inc. store resetter',
            
            'month':now.strftime("%a"),
            'day':now.strftime("%d"),
            'copyright_years':'2010',
            'now':datetime.datetime.now().strftime("%A %d. %B %Y at %I:%M %p"),
            'logout':users.create_logout_url("/"),
            'login':users.create_login_url("/"),
            'user':users.get_current_user(),
            
            'robot':'index, nofollow, noarchive',
            'googlebot':'noarchive',
            'author':'RB&BR',
            'def_selection':'--Make A Selection--',
            'def_name':'Resetter',
            }
    return retval
    
def is_root(user):
    return user.email() == 'richard@bucker.net' \
        or user.email() == 'rbucker881@gmail.com' \
        or user.email() == 'bryonr@att.net' \
        or user.email() == 'brickard10@gmail.com'