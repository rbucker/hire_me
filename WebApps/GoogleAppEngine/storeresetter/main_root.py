#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import cgi
import logging
from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError, TooManyReferences
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.runtime import DeadlineExceededError

import config
import ctool
from models import Partner,User
from main_admin import deactivate, activate,releaseRecord

logger = logging.getLogger()

class MainHandler(webapp.RequestHandler):
    def post(self,mode=''):
        try:
            user = ctool.authorize('root')
            if mode == 'create_partner':
                key = self.request.get('key')
                partner_name = self.request.get('name')
                u = None
                if key:
                    u = Partner.get(key)
                    u.name = partner_name
                else:
                    u = Partner(name=partner_name,is_active=True)
                u.put()
            elif mode == 'create_admin':
                name = self.request.get('name')
                email = self.request.get('email')
                partner_id = self.request.get('partner')
                p = Partner.get(partner_id)
                key = self.request.get('key')
                u = None
                if key:
                    u = User.get(key)
                    u.name = name
                    u.email = email
                    if p.key() != u.partner.key():
                        ctool.releaseReference(u.partner)
                        u.partner = p
                        ctool.getReference(u.partner)
                else:
                    u = User(partner=p,name=name,email=email,is_admin=True,is_active=True)
                    ctool.getReference(p)
                u.put()
            else:
                key = self.request.get('key')
                partner_name = self.request.get('name')
                u = None
                if key:
                    u = Partner.get(key)
                    u.name = partner_name
                else:
                    u = Partner(name=partner_name,is_active=True)
                u.put()
                mode = 'create_partner'
            self.redirect('/root/'+mode)
        except db.BadValueError:
            self.redirect('/error/content_badvalue.html')
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
    def get(self,mode=''):
        try:
            data = {}
            user = ctool.authorize('root')
            if mode == 'create_partner':
                p = Partner.all().order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = Partner.get(key)
                data = {'partner':p,'i':i}
            elif mode == 'delete_admin':
                key = self.request.get('key')
                i = None
                if key:
                    i = User.get(key)
                    releaseRecord(i)                    
                return self.redirect('/root/create_admin')
            elif mode == 'create_admin':
                p = Partner.all().filter('is_active =',True).filter("name != ",config.template_values()['def_name']).order('name')
                u = User.all().filter('is_admin =',True).order('name')
                key = self.request.get('key')
                i = None
                if key:
                    i = User.get(key)
                data = {'partner':p,'users':u,'i':i}
            elif mode == 'delete_partner':
                key = self.request.get('key')
                i = None
                if key:
                    i = Partner.get(key)
                    # we do not release the partner record.
                    i.delete()
                    #releaseRecord(i)
                return self.redirect('/root/create_partner')
            elif mode == 'toggle_admin':
                key = self.request.get('key')
                p = User.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/root/create_admin')
            elif mode == 'toggle_partner':
                key = self.request.get('key')
                p = Partner.get(key)
                if p.is_active:
                    deactivate(p)
                else:
                    activate(p)
                return self.redirect('/root/create_partner')
            else:
                mode = 'root'
            self.render_with_data('content_'+mode+'.html',data)
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except TooManyReferences:
            self.redirect('/error/content_refcount.html')
    def render(self,content):
        self.render_with_data(content,[])
    def render_with_data(self,content,data):
        try:
            user = ctool.authorize('root')
            self.response.out.write(ctool.render_with_data('root',content,data))
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except DeadlineExceededError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("This operation could not be completed in time...")
        except CapabilityDisabledError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("System appears to be be in database maintenance mode...")


def main():
    logging.getLogger().setLevel(logging.DEBUG)
    webapp.template.register_template_library('django_hack')
    application = webapp.WSGIApplication([
                                          ('/root', MainHandler),
                                          ('/root/', MainHandler),
                                          ('/root/(create_partner)', MainHandler),
                                          ('/root/(delete_partner)', MainHandler),
                                          ('/root/(toggle_partner)', MainHandler),
                                          ('/root/(create_admin)', MainHandler),
                                          ('/root/(delete_admin)', MainHandler),
                                          ('/root/(toggle_admin)', MainHandler),
                                          ],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
