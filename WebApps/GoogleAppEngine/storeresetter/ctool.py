from types import *
import os
import logging
from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp import template

import config
from models import User, Partner

logger = logging.getLogger()


def getPartner():
    u = users.get_current_user()
    if u:
        return User.all().filter("email = ",u.email())[0].partner
    else:
        return None
def getPartnerName():
    try:
        p = getPartner()
        return p.name
    except:
        return 'Resetter'
    
def getReference(p):
    if p:
        p.refcount += 1
        p.put()
    return p
    
def releaseReference(p):
    if p and p.refcount > 0:
        p.refcount -= 1
        p.put()
    return p
    
def reReleaseReference(before,after):
    if before and after:
        if before.key() != after.key():
            releaseReference(before)
            getReference(after)
    elif before:
        releaseReference(before)
    elif after:
        getReference(after)
    return after
    
def getPartnerRef():
    return getReference(getPartner())
    
def releasePartnerRef():
    return releaseReference(getPartner())
    
def getRole(user):
    if user:
        q = db.GqlQuery("SELECT * FROM User WHERE owner = :1",user)
        if q.count(1) <= 0:
            if user.email() == 'richard@bucker.net' or user.email() == 'rbucker881@gmail.com' or user.email() == 'bryonr@att.net' or user.email() == 'brickard10@gmail.com':
                p = Partner.all().filter("name = ",config.template_values()['def_name'])
                if p.count() == 1:
                    p = p[0]
                else:
                    p = Partner(name=config.template_values()['def_name'],is_active=True)
                    p.put()
                u = User(partner=p,owner=user,email=user.email(),name=user.nickname(),is_active=True,is_root=True)
                u.put()
                logging.error('There was an error retrieving posts from the datastore')
            else:
                q = db.GqlQuery("SELECT * FROM User WHERE email = :1",user.email())
                if q.count(1) <= 0:
                    raise NotRegisteredError
        for user_role in q:
            if user_role.is_root:
                return 'root'
            if user_role.is_admin:
                return 'admin'
            # 'user' == 'manager'
            return 'user'
        raise NotRegisteredError
    else:
        return 'home'

def authorize(mode):
    if mode != 'home':
        user = users.get_current_user()
        if user:
            user_role = getRole(user)        
            if (mode == 'root' and user_role == 'root') or (mode == 'admin' and user_role == 'admin') or mode == 'user':
                return user_role
        else:
            raise NotLoggedInError
    return True

def render(menu_mode, content):
    return render_with_data(menu_mode, content,[])
def render_with_data(menu_mode, content,data):
    u = users.get_current_user()
    if u and menu_mode!='error':
        role = getRole(u)
        if role == 'root':
            user_mode = 'root'
        elif role == 'admin':
            user_mode = 'admin'
        elif role == 'user':
            user_mode = 'user'
        elif role == 'home':
            user_mode = 'home'
        else:
            raise PermissionDenied
    elif menu_mode=='error':
        menu_mode = 'home'
        user_mode = 'error'
    else:
        # error
        menu_mode = 'home'
        user_mode = 'home'
    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
    #if menu_mode == 'home':
    #    indexpath = os.path.join(os.path.dirname(__file__),'templates', 'index_home.html')
    template_values = config.template_values()
    template_values[menu_mode+'_current'] = 'current'
    template_values['data'] = data
    if type(data) is DictionaryType:
        template_values.update(data)
    template_values['main_menu'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'menu_'+user_mode+'.html'), 
        template_values)
    template_values['about'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'about_'+user_mode+'.html'), 
        template_values)
    template_values['sidebar_menu'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'sidebar_'+menu_mode+'.html'), 
        template_values)
    template_values['post'] = template.render(os.path.join(os.path.dirname(__file__),'templates', content), 
        template_values)
    template_values['contact'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'contact.html'), 
        template_values)
    template_values['footer'] = template.render(os.path.join(os.path.dirname(__file__),'templates', 'footer.html'), 
        template_values)
    retval = template.render(indexpath, template_values)
    return retval

#__END__