#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import logging
from myerr import PermissionDeniedError, NotLoggedInError, NotRegisteredError, TooManyReferences, PageNotFound
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.runtime import DeadlineExceededError

import config
import ctool
from models import Client, State, Store, Project, Shift, User, Partner, Rate, Employee, Assignment, Week, Timesheet, Expense

class MainHandler(webapp.RequestHandler):
    def post(self,mode=''):
        try:
            user = ctool.authorize('user')
            data = {}
            if mode == 'alerts':
                pass
            elif mode == 'timesheet':
                pass
            elif mode == 'expense':
                shift_id = self.request.get('this_shift')
                shift = Shift.get(shift_id)
                week_id = self.request.get('this_week')
                week = Week.get(week_id)
                description = self.request.get('description')
                amount = self.request.get('amount')
                key = self.request.get('key')
                u = None
                if key:
                    u = Expense.get(key)
                    ctool.reReleaseReference(u.shift,shift)
                    u.shift = shift
                    ctool.reReleaseReference(u.week_ending,week)
                    u.week_ending = week
                    u.description = description
                    u.amount = float(amount)
                else:
                    u = Expense(partner=ctool.getPartnerRef(),shift=shift,week_ending=week,description=description,amount=float(amount))
                    ctool.getReference(u.shift)
                    ctool.getReference(u.week_ending)
                u.put()
                return self.redirect("/user/expense?this_shift="+shift_id+"&this_week="+week_id)
            else:
                raise PageNotFound
            self.redirect('/user/'+mode)
        #except db.BadValueError:
        #    self.redirect('/error/content_badvalue.html')
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except PageNotFound:
            self.redirect('/error/content_pagenotfound.html')
    def get(self,mode='user'):
        data = {}
        try:
            if mode=='timesheet':
                pass
            elif mode=='expense':
                shifts = Shift.all().filter("partner = ",ctool.getPartner()).filter("is_active = ",True).order('name')
                weeks = Week.all().filter("partner = ",ctool.getPartner()).filter("is_active = ",True).order('week_ending')
                this_shift_id = self.request.get('this_shift')
                this_week_id = self.request.get('this_week')
                expenses = None
                is_locked = False
                this_shift = None
                if this_shift_id:
                    this_shift = Shift.get(this_shift_id)
                this_week = None
                if this_week_id:
                    this_week = Week.get(this_week_id)
                if this_shift_id and this_week_id:
                    expenses = Expense.all().filter("partner = ",ctool.getPartner()).filter("shift = ",this_shift).filter("week_ending = ",this_week).filter("is_active = ",True)
                    if expenses and expenses.count() > 0:
                        is_locked = expenses[0].is_locked
                key = self.request.get('key')
                i = None
                if key and not is_locked:
                    i = Expense.get(key)
                data = {'expenses':expenses,'shifts':shifts,'weeks':weeks,'this_shift':this_shift,'this_week':this_week,'i':i,'role':'user','is_locked':is_locked}
            elif mode=='lock_expense':
                shift_id = self.request.get('this_shift')
                week_id = self.request.get('this_week')
                this_shift = None
                if shift_id:
                    this_shift = Shift.get(shift_id)
                this_week = None
                if week_id:
                    this_week = Week.get(week_id)
                if this_shift and this_week:
                    expenses = Expense.all().filter("partner = ",ctool.getPartner()).filter("shift = ",this_shift).filter("week_ending = ",this_week).filter("is_active = ",True).filter("is_locked = ",False)
                    if expenses and expenses.count() > 0:
                        for e in expenses:
                            e.is_locked = True
                            e.put()
                return self.redirect('/user/expense?this_shift='+shift_id+'&this_week='+week_id)
            elif mode=='delete_expense':
                key = self.request.get('key')
                shift_id = self.request.get('this_shift')
                week_id = self.request.get('this_week')
                i = None
                if key:
                    i = Expense.get(key)
                    ctool.releaseReference(i.shift)
                    ctool.releaseReference(i.week_ending)
                    releaseRecord(i)
                return self.redirect('/user/expense?this_shift='+shift_id+'&this_week='+week_id)
            elif mode=='alerts':
                pass
            elif mode=='user':
                pass
            else:
                raise PageNotFound
            self.render_with_data('content_'+mode+'.html',data)
        except db.BadValueError:
            self.redirect('/error/content_badvalue.html')
        except db.BadKeyError:
            self.redirect('/error/content_badvalue.html')
        except TooManyReferences:
            self.redirect('/error/content_refcount.html')
    def render(self,content):
        self.render_with_data(content,[])
    def render_with_data(self,content,data):
        try:
            user = ctool.authorize('user')
            self.response.out.write(ctool.render_with_data('user',content,data))
        except NotRegisteredError:
            self.redirect('/error/content_notregistered.html')
        except PermissionDeniedError:
            self.redirect('/error/content_permissiondenied.html')
        except NotLoggedInError:
            self.redirect('/error/content_notloggedin.html')
        except DeadlineExceededError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("This operation could not be completed in time...")
        except CapabilityDisabledError:
            # FIXME: log this
            self.response.clear()
            self.response.set_status(500)
            self.response.out.write("System appears to be be in database maintenance mode...")

def main():
    logging.getLogger().setLevel(logging.DEBUG)
    application = webapp.WSGIApplication([
                                        ('/user', MainHandler),
                                        ('/user/(.*)', MainHandler),
                                        ],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
