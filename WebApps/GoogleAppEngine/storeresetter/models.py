import logging, datetime
from google.appengine.ext import db


class Log(db.Model):
    log_date = db.DateTimeProperty(auto_now_add=True)
    message = db.StringProperty(required=True)

class Partner(db.Model):
    name = db.StringProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)

class User(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    owner = db.UserProperty()
    #uid = db.StringProperty(required=True)
    #pwd = db.StringProperty(required=True)
    name = db.StringProperty(required=True)
    email = db.EmailProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    is_admin = db.BooleanProperty(required=True,default=False)
    is_root = db.BooleanProperty(required=True,default=False)
    is_manager = db.BooleanProperty(required=True,default=False)
    refcount = db.IntegerProperty(required=True,default=0)

class Company(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    name = db.StringProperty(required=True)
    addr1 = db.StringProperty(required=True)
    addr2 = db.StringProperty(required=True)
    city = db.StringProperty(required=True)
    state = db.StringProperty(required=True)
    zip = db.StringProperty(required=True)
    phone = db.StringProperty(required=True)
    refcount = db.IntegerProperty(required=True,default=0)

class Client(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    #company = db.ReferenceProperty(Company)
    name = db.StringProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    #contact = db.StringProperty(required=True)
    #phone = db.StringProperty(required=True)
    #email = db.EmailProperty(required=True)
    refcount = db.IntegerProperty(required=True,default=0)

class State(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    name = db.StringProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)

class Store(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    #company = db.ReferenceProperty(Company)
    client = db.ReferenceProperty(Client,required=True)
    state = db.ReferenceProperty(State,required=True)
    manager = db.ReferenceProperty(User,required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    name = db.StringProperty(required=True)
    #addr1 = db.StringProperty(required=True)
    #addr2 = db.StringProperty(required=True)
    #city = db.StringProperty(required=True)
    #state = db.StringProperty(required=True)
    #zip = db.StringProperty(required=True)
    #contact = db.StringProperty(required=True)
    #phone = db.StringProperty(required=True)
    #email = db.EmailProperty(required=True)
    refcount = db.IntegerProperty(required=True,default=0)
    
class Project(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    store = db.ReferenceProperty(Store)
    name = db.StringProperty()
    is_active = db.BooleanProperty(required=True,default=True)
    #start_date = db.DateTimeProperty()
    #end_date = db.DateTimeProperty()
    refcount = db.IntegerProperty(required=True,default=0)

class Shift(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    project = db.ReferenceProperty(Project)
    name = db.StringProperty()
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)
    
class Rate(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    name = db.StringProperty()
    amount = db.FloatProperty()
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)
    
class Employee(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    firstname = db.StringProperty(required=True)
    mi = db.StringProperty(required=False)
    lastname = db.StringProperty(required=True)
    rate = db.ReferenceProperty(Rate,required=True)
    addr1 = db.StringProperty(required=True)
    addr2 = db.StringProperty(required=False)
    city = db.StringProperty(required=True)
    state = db.StringProperty(required=True)
    zip = db.StringProperty(required=True)
    phone1 = db.StringProperty(required=True)
    phone2 = db.StringProperty(required=False)
    nontravel_employee = db.BooleanProperty(required=True,default=False)
    paystub_required = db.BooleanProperty(required=True,default=False)
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)
    perdiem = db.FloatProperty(required=False,default=0.0)
    buddy = db.SelfReferenceProperty(required=False)
    shift = db.ReferenceProperty(Shift,required=False,default=None)

class Assignment(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    employee  = db.ReferenceProperty(Employee,required=True)
    shift  = db.ReferenceProperty(Shift,required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)

class Week(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    week_ending = db.DateProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    refcount = db.IntegerProperty(required=True,default=0)

class Timesheet(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    employee = db.ReferenceProperty(Employee)
    shift = db.ReferenceProperty(Shift)
    week_ending = db.ReferenceProperty(Week)
    hours = db.ListProperty(float)
    is_active = db.BooleanProperty(required=True,default=True)
    is_locked = db.BooleanProperty(required=True,default=False)
    refcount = db.IntegerProperty(required=True,default=0)
    
class Expense(db.Model):
    partner = db.ReferenceProperty(Partner,required=True)
    shift = db.ReferenceProperty(Shift)
    week_ending = db.ReferenceProperty(Week)
    description = db.StringProperty(required=True)
    amount = db.FloatProperty(required=True)
    is_active = db.BooleanProperty(required=True,default=True)
    is_locked = db.BooleanProperty(required=True,default=False)
    refcount = db.IntegerProperty(required=True,default=0)

## __END__
