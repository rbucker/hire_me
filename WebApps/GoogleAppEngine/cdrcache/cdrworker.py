import wsgiref.handlers
from google.appengine.api.labs import taskqueue
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

import datetime

class CDRFile(db.Model):
    filename = db.StringProperty(multiline=True,required=True)
    datadomain = db.StringProperty(required=True)
    filetype = db.StringProperty(required=True)
    inserted = db.DateTimeProperty(auto_now_add=True)
    contents = db.BlobProperty()

class CDRFileHandler(webapp.RequestHandler):
    def get(self):
        """
        return the number of tasks in the queue??
        """
        self.response.out.write(template.render('cdrworker.html',{}))
        #self.response.out.write('nothing to report ')

    def post(self):
        """
        save the file in the datastore, then add a task to the queue
        """
        key = self.request.get('key')
        if key == None:
            self.response.out.write(template.render('cdrworker.html',{}))
        else:
            def txn():

                inserted = datetime.datetime.now()
                datadomain = self.request.get('datadomain')
                filetype = self.request.get('filetype')
                filename = self.request.get('filename') 
                cdr = CDRFile(filetype=filetype,filename=filename,datadomain=datadomain)
                f= self.request.get('file')
                cdr.contents = db.Blob(f) 
                cdr.put()
                t = taskqueue.Task(url='/cdrworker/do/', method='GET',params={'key': cdr.key()})
                t.add('cdr-processing')
            db.run_in_transaction(txn)
        self.redirect('/')

class CDRFileWorker(webapp.RequestHandler):
    def get(self): # should run at most 1/s
        """
        get the next task from the queue, retrieve the file from the CDRFile datastore, 
        then load the CDRs into the CDRRecord datastore
        """
        key = self.request.get('key')
        def txn():
            cdrfile = CDRFile.get(key)
            cdrfile.delete()
        db.run_in_transaction(txn)

def main():
    wsgiref.handlers.CGIHandler().run(webapp.WSGIApplication([
        ('/cdrworker/add/', CDRFileHandler),
        ('/cdrworker/do/', CDRFileWorker),
    ]))

if __name__ == '__main__':
    main()

# __END__
