#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#




import wsgiref.handlers

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp import template
import os

import admin

class MainHandler(webapp.RequestHandler):

  def get(self):
    path = os.path.join(os.path.dirname(__file__),'templates', 'index.html')
    template_values = {
            'title':'WEB-HSM',
            'base_url':'http://hsm.cdrcache.net',
            'client_name':'CDR Cache',
            '':'',
            'copyright_years':'2009',
            'copyright':'CDR Cache',
            'copyright_url':'http://www.cdrcache.com',
            }
    self.response.out.write(template.render(path, template_values))

class MainPage(webapp.RequestHandler):
    @login_required
    def get(self):
        user = users.get_current_user()

        if user:
            self.response.headers['Content-Type'] = 'text/html'
            self.response.out.write('Hello, ' + user.nickname())
            if users.is_current_user_admin():
                self.response.out.write('   | <a href="/admin/">Admin</a>')
            self.response.out.write('   | <a href="'+users.create_logout_url('/')+'">LogOut</a>')
        else:
            self.redirect(users.create_login_url(self.request.uri))


def main():
  application = webapp.WSGIApplication([
                                       ('/home', MainPage),
                                       ('/', MainHandler),
                                       ],
                                       debug=True)
  wsgiref.handlers.CGIHandler().run(application)


if __name__ == '__main__':
  main()
