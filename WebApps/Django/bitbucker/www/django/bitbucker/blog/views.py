# Create your views here.
from django.shortcuts import render_to_response
from django.conf import settings
from django.contrib.auth.decorators import login_required

import logging
logger = logging.getLogger()

def index(request):
    logger.debug("%s.index()" % (__name__))
    custom = settings.CUSTOM
    return render_to_response('blog/index.html', locals())

#__END__
