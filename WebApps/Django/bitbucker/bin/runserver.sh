#!/usr/bin/env bash


DIR=`pwd`/`dirname ${0}`
WEBAPP="${DIR}/../www/django/bitbucker"
LIB="${WEBAPP}/lib"

PYTHONPATH="${PYTHONPATH}:${LIB}"

echo "dir: "$DIR
echo "webapp: "$WEBAPP
echo "lib: "$LIB
echo "pythonpath: "$PYTHONPATH

cd $WEBAPP
./manage.py runserver localhost:8000

#__END__
