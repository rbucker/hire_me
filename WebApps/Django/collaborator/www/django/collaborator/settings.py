# ref: http://code.djangoproject.com/wiki/SplitSettings#UsingalistofconffilesTransifex

import os
import glob
conffiles = glob.glob(os.path.join(os.path.dirname(__file__), 'settings', '*.conf'))
conffiles.sort()
for f in conffiles:
    if f.endswith('-user-local.conf') or f.endswith('-'+os.getlogin()+'-local.conf'):
        print "Loading config file: %s" % (f)
        execfile(os.path.abspath(f))
    elif not f.endswith('-local.conf'):
        print "Loading config file: %s" % (f)
        execfile(os.path.abspath(f))
        

# __END__

