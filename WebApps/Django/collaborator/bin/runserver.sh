#!/usr/bin/env bash


DIR=`pwd`/`dirname ${0}`
WEBAPP="${DIR}/../www/django/collaborator"
LIB="${WEBAPP}/lib"

PYTHONPATH="${PYTHONPATH}:${LIB}"

echo $DIR
echo $WEBAPP
echo $LIB
echo $PYTHONPATH

cd $WEBAPP
./manage.py runserver localhost:8000

#__END__
