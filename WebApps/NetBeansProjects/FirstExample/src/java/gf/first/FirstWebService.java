/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gf.first;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ejb.Stateless;

import java.lang.String;

/**
 *
 * @author rbucker
 */
@WebService()
@Stateless()
public class FirstWebService {

    boolean isNull(Object o) {
        return o==null || (o instanceof java.lang.String && ((java.lang.String)o).length() == 0);
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "create_account")
    public String create_account(@WebParam(name = "name")
    String name, @WebParam(name = "account")
    String account, @WebParam(name = "amount")
    float amount) {
        //TODO write your implementation code here:
        return isNull(name) || isNull(account)  ? "invalid" : "valid";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "update_account")
    public String update_account(@WebParam(name = "name")
    String name, @WebParam(name = "account")
    String account, @WebParam(name = "amount")
    float amount) {
        //TODO write your implementation code here:
        return null;
    }

    public void businessMethod(String name, String account, float amount) {
    }

}
