Title: Customary README

Author: Richard Bucker

Date: March 3, 2012

Copyright: Copyright (c) 2012 Richard Bucker

Email: richard@bucker.net

Web: http://richardbucker.com


## Welcome ##

This project has been created in response to the many recent articles that have been written and the number of job search services which feed on public source repositories. Part of me wants to find a novel way to game the system but I have a higher purpose and that's the message I want to get out.

If you want to know more about my writing you can always head over to my blog. (http://richardbucker.com). Depending on when inspiration hits I tend to write a new article about once a week and I cover a wide variety of topics.

As a general rule, however, while I have my opinions I have a good sense between what is opinion or intuition and what is fact. My training and application of _root cause analysis_ has been an eye-opener in knowing the difference.

## Goals ##

My goal is to land the perfect job in the perfect environment for the perfect company. I'm not particular about startup or established, fulltime or contract, or the vertical market, programming language, project management style... although I have my favorites. My mission is to provide a high rate of ROI.

## Next ##

Over the next few days I'm going to populate this project with code that I've written specifically for this purpose. Some of it will run and some will be gists.

## Warning ##

The amount of time I put into this project is going to be completely inversely proportional to what happens in real life project development. The application of comments, selection of variable names, even sample code complexity; such nuance are more subjective than ever.

## Thank You ##

Thank you for taking the time to read through this project. If you're a hiring manager or just a passer-by; if you have some comments I hope you'll forward them to me (richard@bucker.net).

## Copyright ##

The content of this website is *Copyright (c) 2012, Richard Bucker*. I'd like to share and I believe that the *public* nature/license of this repo is that it be fork-able. So have at it, however, please keep my copyright in tact.