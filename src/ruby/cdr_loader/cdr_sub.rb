#
#  Copyright (c) 2012, Richard Bucker
#  All Rights Reserved
#

# This is the basic workflow:
#
#     open a connection to redis
#     subscribe to the cdr_pub channel
#     convert the message to a hash
#     format a CDR string
#     append the CDR message to the CDR file
#
# NOTE:
#     the CDR filename is specially formatted
#     the file(s) rotate every so often (approx 5 min)
#
# TBD:
#     some day the subscriber will be on the dashboard and import immediately.
#

require 'rubygems'
require 'redis'
require 'json'
require 'syslog'

$file_path = '/cdrs/'

def log(message)
  # $0 is the current script name
  Syslog.open($0, Syslog::LOG_PID | Syslog::LOG_CONS) { |s| s.warning message }
end

def save_cdr(data)
  # 201203082125-64.34.161.119-cdr.txt
  now = Time.new
  y = now.year
  m = now.month
  d = now.day
  h = now.hour
  min = now.min
  // TLD of .local behaves badly on OSX systems. Need a better name.
  ipaddr = IPSocket.getaddress("switch.local").inspect.tr_s('"','')
  puts ipaddr
  filetimestamp = sprintf("%04d%02d%02d%02d%02d", y, m, d, h, min)
  cdrfile = sprintf("%s%s-%s-cdr.txt", $file_path, filetimestamp, ipaddr)
  cdr = sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
          data["uid"],
          data["2"], # client
          data["3"], # supplier
          data["4"], # start
          data["5"], # duration
          data["6"], # billsec
          data["7"], # disposition
          data["8"], # cause
          data["9"], # network
          data["agi_channel"],
          data["agi_type"],
          data["agi_uniqueid"],
          data["agi_callerid"],
          data["agi_calleridname"],
          data["agi_dnid"],
          data["agi_rdnis"],
          data["agi_extension"],
          data["agi_accountcode"],
          data["1"],
          data["switch_ip"],
          data["10"], #lastapp
          data["11"] # dialed
         )
  begin
    File.open(cdrfile, "a") do |file|
      file.write(cdr)
    end
  rescue Exception
    log(sprintf("Error writing CD to file(%s): #{$!}",cdrfile))
    log(cdr)
  end
end


begin
  $redis = Redis.new(:timeout => 0)

  $redis.subscribe('cdr_pub', 'ruby-lang') do |on|

    on.message do |channel, msg|
      data = JSON.parse(msg)
      puts "##{channel} - #{data}"
      save_cdr(data)
    end
  end
rescue Exception
  log("Error connecting/processing to Redis: #{$!}")
end


# __END__
