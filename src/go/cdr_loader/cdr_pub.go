/*
  Copyright (c) 2012, Richard Bucker
  All Rights Reserved

  NOTE: This dev has been suspended because the pub/sub
  I was planning on using... either beanstalkd or redis
  has very old client libs. So I'm postponing the dev
  of this project for the moment.
*/
package main

import "os"
import "bytes"
import "flag"
import "fmt"
import "json"
import "bufio"
import "strconv"
import "strings"

//import "github.com/kr/beanstalk.go.git"
import "redis"
//import "syslog"

// Read a whole file into the memory and store it as array of lines
func readLines(path string) (lines []string, err os.Error) {
    var (
        file *os.File
        part []byte
        prefix bool
    )
    if file, err = os.Open(path); err != nil {
        return
    }
    reader := bufio.NewReader(file)
    buffer := bytes.NewBuffer(make([]byte, 1024))
    for {
        if part, prefix, err = reader.ReadLine(); err != nil {
            break
        }
        buffer.Write(part)
        if !prefix {
            lines = append(lines, buffer.String())
            buffer.Reset()
        }
    }
    if err == os.EOF {
        err = nil
    }
    return
}

func main() {
	fmt.Println("Hello, 世界")

  // create a local storage
  var sto = map[string] string {}

  // store the command line options in the storage
  flag.Parse()
  if flag.NArg() > 0 {
    i := 1
    for _, arg := range flag.Args() {
      //fmt.Printf("%s\n", arg)
      sto[strconv.Itoa(i)] = arg
      i++
    }
  }
  if flag.NArg() <= 0 {
    fmt.Println("no data")
  }

  // read STDIN and convert the data to storage
  argv, _ := readLines("/dev/stdin")
  for _, arg := range argv {
    //fmt.Printf("%s\n",arg)  
    res := strings.Split(arg, ":")
    key, val := strings.Trim(res[0], "\u0000 "), strings.Trim(res[1], "\u0000 ")
    sto[key] = val
  }
  // convert the local storage to json or an sexp
  //b, _ := json.MarshalIndent(sto, "", "  ")
  b, _ := json.Marshal(sto)
  fmt.Printf("%s", b)  
  //fmt.Println(sto)  

  // pub the message to the local redis instance
  // if there is an error then send the msg to syslog

}
// __END__
