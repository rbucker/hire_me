natsworker
==========

INSTALL DEPS
============

go get github.com/apcera/gnatsd
gnatsd

go  get github.com/apcera/nats

EXAMPLE
=======

I was going to write a simple pub/sub like I have for the other projects but
Apcera already did that in 2012. No point in redoing it.

__END__
