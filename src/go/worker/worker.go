package main

import (
	"fmt"
	"strings"
	"sync"
)

type Context struct {
	Request  string
	Response string
	TaskID   int
	WorkerID int
	RspChan  chan Context
}
type RspChan chan Context
type ReqChan chan Context

var workers int = 4

func main() {
	var wg sync.WaitGroup
	reqchan := make(ReqChan, 200)
	rspchan := make(RspChan)
	// start receiver
	go func(rsp RspChan) {
		for {
			select {
			case ctx := <-rsp:
				fmt.Println(ctx)
				wg.Done()
			}
		}
	}(rspchan)
	// start workers
	for i := 0; i < workers; i++ {
		go func(i int, req ReqChan, rsp RspChan) {
			for {
				select {
				case ctx := <-req:
					ctx.WorkerID = i
					fmt.Println("work", ctx)
					ctx.Response = strings.ToUpper(ctx.Request)
					rsp <- ctx
				}
			}

		}(i, reqchan, rspchan)
	}
	// send some work
	for j := 0; j < 100; j++ {
		ctx := Context{TaskID: j, Request: "hello"}
		wg.Add(1)
		reqchan <- ctx
	}
	// wait for
	fmt.Println("waiting")
	wg.Wait()
}

// __END__
