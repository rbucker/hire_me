package main

import (
	"fmt"
	"sync"

	"code.google.com/p/go.exp/old/netchan"
)

type Context struct {
	Request  string
	Response string
	TaskID   int
	WorkerID int
	RspChan  chan Context
}

func main() {
	var wg sync.WaitGroup
	req := make(chan Context)
	rsp := make(chan Context)
	chrsp := netchan.NewExporter()
	chrsp.Export("boss", rsp, netchan.Recv)
	chrsp.ListenAndServe("tcp", ":8081")
	ch, err := netchan.Import("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
	}
	ch.Import("worker", req, netchan.Send, 10)
	// start receiver
	go func() {
		for {
			select {
			case ctx := <-rsp:
				fmt.Println(ctx)
				wg.Done()
			}
		}
	}()

	// send some work
	for j := 0; j < 10; j++ {
		ctx := Context{TaskID: j, Request: "hello"}
		wg.Add(1)
		req <- ctx
	}
	// wait for
	fmt.Println("waiting")
	wg.Wait()
}

// __END__
