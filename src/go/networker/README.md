networker
=========

it's an interesting idea to have named channels move messages. The challenge is
routing to the correct worker. This requires a proper MQ where the target IP/PORT
is shared. Or some sort of ephemeral socket.

ZeroMQ is a good choice because it supports multiple topologies. The only challenge
is that a wrapper of some kind is a good idea so that you change the topology all
at once.


