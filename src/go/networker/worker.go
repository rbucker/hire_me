package main

import (
	"fmt"
	"strings"
	"code.google.com/p/go.exp/old/netchan"
)

type Context struct {
	Request  string
	Response string
	TaskID   int
	WorkerID int
	RspChan  chan Context
}

func main() {
	req := make(chan Context)
	rsp := make(chan Context)
	ch := netchan.NewExporter()
	ch.Export("worker", req, netchan.Recv)
	ch.ListenAndServe("tcp", "127.0.0.1:8080")
		for {
			select {
			case ctx := <-req:
				chrsp, err := netchan.Import("tcp", ":8081")
				if err != nil {
					fmt.Println(err)
				}
				chrsp.Import("boss", rsp, netchan.Send, 10)
				ctx.WorkerID = 1
				fmt.Println("work", ctx)
				ctx.Response = strings.ToUpper(ctx.Request)
				rsp <- ctx
			}
		}

}

// __END__
