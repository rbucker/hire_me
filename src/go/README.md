Workers
=======

It's never a good idea to post dead projects except that I've been deep diving
into MQs and channels for a while. As such I have been investigating many
different libraries and approaches.

zeromq
libchan
netchan
mbxchan
nats
nsq
nanomsg (mangos)

none of them are truly outstanding, however, I like the implementation of netchan in
that the channel is linked to the socket and vice versa. It makes things very clear.

mbxchan is trash in that it is extremely opinionated about the workspace structure
and in that way it not idiomatic.

ZeroMQ seems to be losing traction. The libs are dated and at least one project is
stale.

libchan is new and poorly documented.

nsq docker files are getting stale. Reading an ISSUE the topic seemed to suggest
it's not that well thought out.

netchan is deprecated by the golang team.

nanomsg (mangos) seems to have dealt with the topology issue and there is a native go 
implementation (mangos). But it's hard to work with.


