#!/usr/bin/env python
"""
Copyright (c) 2012 Richard Bucker
All Rights Reserved

Here is a sample program that processes argv[1] as a CSV file and
parses the record looking for a first and last name. That name is
then used to lookup the address in the Broward County FL County 
Clerk Website. The results are then stored in a Google Doc spreadsheet
for further processing. (this is only part of the process)

This code actually processes two different types of input files.
The common type is CSV that is produced by the Broward County Clerk.
The results of both are sent into the Broward County Appraiser's
office in order to get a potential address.
"""

# standard python libs
import os
import re
import csv
import sys
import time
import urllib
import urllib2
import datetime
from collections import deque

# 3rd party libs
from BeautifulSoup import BeautifulSoup
import gdata.spreadsheet.service

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#
# specify the skip_name filter.
# skip_name = ''

try:
    # if the patter above is present then this is going to work
    pattern = re.compile(skip_name, re.IGNORECASE)
except:
    pattern = ''
try:
    fpattern = re.compile('URL_Seq=(\d+)', re.IGNORECASE)
except:
    fpattern = ''

os.environ['TZ'] = 'US/Eastern'
time.tzset()

email = 'richard@friedlawgroup.com'
password = 'XXXXXXXXX'
# Find this value in the url with 'key=XXX' and copy XXX below
spreadsheet_key = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
# All spreadsheets have worksheets. I think worksheet #1 by default always
# has a value of 'od6'
worksheet_id = 'od6'

spr_client = gdata.spreadsheet.service.SpreadsheetsService()
spr_client.email = email
spr_client.password = password
spr_client.source = 'get_address'
spr_client.ProgrammaticLogin()

#
#
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def process(recinfo):
    """Given a webpage, scrape the info from the page then
    insert the important data into the Google spreadsheet.
    """
    found = 0
    html = recinfo
    soup2 = BeautifulSoup(html)
    ma = soup2.find('span',text=re.compile('.*Property Owner.*'))
    # ma.strip()
    try:
        owner = ma.findNext('span').text
    except:
        print "I do not know this person"
        return 0
    owner = owner.strip()

    if pattern and not pattern.match(owner):
        #print "SKIPPING: ", owner
        return 0
    
    ma = soup2.find('span',text=re.compile('.*Mailing Address.*'))
    # ma.strip()
    address = ma.findNext('span').text
    addr = address.split('&nbsp;')

    # more debug info
    print "+++++++++++++++++++++++++"
    print "OWNER:", owner
    for a in addr:
        print a


    # Prepare the dictionary to write in the spreadsheet
    dict = {}
    dict['owner'] = owner
    now = datetime.datetime.now()
    dict['date'] = now.strftime('%m/%d/%Y')
    dict['time'] = now.strftime('%H:%M:%S')
    dict['addr1'] = addr[0]
    if len(addr) > 4:
        dict['addr2'] = addr[-3]
    else:
        dict['addr2'] = ''
    dict['city'] = addr[-3]
    dict['state'] = addr[-2]
    dict['zip'] = str(addr[-1])

    # debug
    print dict

    entry = spr_client.InsertRow(dict, spreadsheet_key, worksheet_id)
    if isinstance(entry, gdata.spreadsheet.SpreadsheetsList):
        print "Insert row succeeded."
        return 1
    else:
        print "Insert row failed."
        return 0


def search(name):
    """POST the search data to the URL, retrieve as many results as there are.
    TODO: This code can be replaced with "pip install requests"
    TODO: IOC is better here instead of nesting
    """
    found = 0
    prev = 0
    params = urllib.urlencode({'Name': name.upper()})
    url = "http://bcpa.net/RecSearch.asp"
    print url
    print params
    flist = urllib2.urlopen(url, params).read()
    while True:
        fsoup = BeautifulSoup(flist)
        t = fsoup.findAll('table', id='Table8')
        try:
            folios = re.findall(r"URL_Folio\=(\w+)", str(t[0]))
            for folio_no in folios:
                url = "http://bcpa.net/RecInfo.asp?URL_Folio=%s" % (folio_no)
                html = urllib2.urlopen(url).read()
                # better to pass "process" in as a param
                found += process(html)
        except:
            found += process(flist)
            

        # GET MORE
        #http://bcpa.net/RecSearch.asp?URL_Sort=Name&URL_Seq=50&URL_Name=FRIED
        qseqs = str(fsoup.findAll('a',href=re.compile(r'^RecSearch.asp\?.*URL_Seq=(\d+)')))
        seqs = re.findall(r"URL_Seq\=(\d+)", qseqs)
        s = 0
        for seq in seqs:
            if int(seq) > s:
                s = int(seq)
        if s == 0 or s <= prev:
            break
        prev = s
        params = urllib.urlencode({'URL_Sort': 'Name', 'URL_Seq': s, 'URL_Name': name.upper()})
        url = "http://bcpa.net/RecSearch.asp?%s" % (params)
        print url
        flist = urllib2.urlopen(url).read()
    return found


if __name__ == "__main__":
    filename = sys.argv[1]
    if filename.endswith('.csv'):
        lisPen = csv.reader(open(filename, 'rb'), delimiter=',', quotechar='"')
        for row in lisPen:
            search(row[5])
    elif filename.endswith('.txt'):
        contents = open(filename, 'rb').read()
        defs = re.findall(r'Vs\.(.{1,300}?) Defendant',contents,re.S)
        found = 0
        for d in defs:
            d = d.replace('Runner','')
            d = re.sub(r'[\r\n\t]','',d)
            d = d.strip()
            #d = ' '.join(re.split(r'\W+',d))
            d = re.sub(r'[ ]+',' ',d)
            dq = 0
            if not re.match(r'Unknown Heirs',d):
                print d, " [raw]"
                dq =  search(d)
                found += dq
                if dq:
                    continue
                t = d.split(' ')
                if not re.match(r'.*[0-9!@\#\$%\^&*\(\)_+=-]+.*',d):
                    deq = deque(t)
                    for i in range (0,len(t)):
                        deq.rotate(1)
                        l = list(deq)
                        s = l[0] + ',' + ' '.join(l[1:])
                        print s, " [rotate]"
                        dq =  search(s)
                        found += dq
                        if dq:
                            break
                else:
                    for i in range(1,(len(t)-1)):
                        n = i * -1
                        s = ' '.join(t[:n])
                        print s, " [shift]"
                        dq =  search(s)
                        found += dq
                        if dq:
                            break
                
        print "+++++++"
        print len(defs), " Defendants"
        print found, " finds"
    else:
        print "I do not recognize the filetype; sorry"

# __END__
