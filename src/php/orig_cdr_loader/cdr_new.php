#!/usr/bin/php
<?php
    pcntl_signal(SIGHUP, SIG_IGN);
    $agivars = array();
    while (!feof(STDIN)) {
        $agivar = trim(fgets(STDIN));
        if ($agivar === '') {
            break;
        }
        $agivar = explode(':', $agivar);
        $agivars[$agivar[0]] = trim($agivar[1]);
    }
    extract($agivars);
   
    define ("PATH", "/cdrs/");
  
    // get the time and store it for use
    $t = time();
    $cur_year = date("Y",$t);
    $cur_mo = date("m",$t);
    $cur_day = date("d",$t);
    $cur_hr = date("H",$t);
    $cur_min = date("i",$t);

    $filetimestamp = date("YmdHi",mktime($cur_hr,$cur_min,0,$cur_mo,$cur_day,$cur_year));
    // make sure that switch.local is in the /etc/hosts
    $switch_ip = gethostbyname('switch.local');

    $filename = PATH . $filetimestamp . '-' . $switch_ip  . "-cdr.txt";
    $f = fopen($filename,'a');

    $row = sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
      $uid,
      $argv[2],	// client
      $argv[3], // supplier
      $argv[4], // start
      $argv[5], // duration
      $argv[6], // billsec
      $argv[7], // disposition
      $argv[8], // cause
      $argv[9], // network
      $agi_channel,
      $agi_type,
      $agi_uniqueid,
      $agi_callerid,
      $agi_calleridname,
      $agi_dnid,
      $agi_rdnis,
      $agi_extension,
      $agi_accountcode,
      $argv[1],
      $switch_ip,
      $argv[10], //lastapp
      $argv[11] // dialed
    );
    fwrite($f,$row);
    fclose($f);

?>


    
