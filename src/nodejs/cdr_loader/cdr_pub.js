/*
	Copyright (c) 2012, Richard Bucker
	All Rights Reserved



NOTE: it appears that there is a bug that was corrected 3 days ago
but has not been pushed to the main version yet. Unfortunately it's
a bug in  READLINE in the stdin. I could look at the source by I'm not.
The fact that yet another language has FAILed is discouraging.








     This is the basic workflow:
    
          create a local storage (hash)
          store the command line options in the storage
          read STDIN and convert the data to storage
          convert the local storage to json or an sexp
          pub the message to the local redis instance
          if there is an error then send the msg to syslog
 */
var util = require('util');
//var readline = require('readline'),
sto = {}

process.argv.forEach(function (val, index, array) {
  	console.log(index + ': ' + val);
});

/*
process.stdin.resume();
process.stdin.on('data', function (chunk) {
  process.stdout.write('data: ' + chunk);
});
*/


/*
rl = readline.createInterface(process.stdin, process.stdout);
process.stdin.resume();
rl.on('line', function (cmd) {
  console.log('You just typed: '+cmd);
}).on('close', function() {
  console.log('Have a great day!');
  process.exit(0);
});
rl.resume();
*/

var readline = require('readline'),
  rl = readline.createInterface(process.stdin, process.stdout, null),
  prefix = 'OHAI> ';

rl.on('line', function(line) {
  switch(line.trim()) {
    case 'hello':
      console.log('world!');
  break;
    default:
      console.log('Say what? I might have heard `' + line.trim() + '`');
      break;
  }
  rl.setPrompt(prefix, prefix.length);
  rl.prompt();
}).on('close', function() {
  console.log('Have a great day!');
  process.exit(0);
});
console.log(prefix + 'Good to see you. Try typing stuff.');
rl.setPrompt(prefix, prefix.length);
rl.prompt();
console.log(util.format("%j",sto));