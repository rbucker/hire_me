--  Copyright (c) 2012, Richard Bucker
--  All Rights Reserved
--

require "json"
require "redis"

-- This is the basic workflow:
--
--      create a local storage (hash)
--      store the command line options in the storage
--      read STDIN and convert the data to storage
--      convert the local storage to json or an sexp
--      pub the message to the local redis instance
--      if there is an error then send the msg to syslog
sto = {}

for i,v in ipairs(arg) do
	sto[tostring(i)] = v
end

while true do
	local line = io.read()
	if line == nil then break end
	-- this method is overkill.
	for k, v in string.gmatch(line, "(%w+):(.+)") do
	   sto[k] = v
	end
end

msg = json.encode(sto)
print(msg)

local redis = Redis.connect('127.0.0.1', 6379)
redis:publish('cdr_pub', msg)

--[[
	the code for this module is complete but
	this project failed for very similar reasons
	to the golang version. the redis code depended
	on the socket lib which was not compiled for
	Lua 5.2 and it had not been updated in nearly
	5 years. This is not the way to build a project.
	I did not want to fork the socket code.
]]--





-- __END__


