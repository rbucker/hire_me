//  Copyright (c) 2012, Richard Bucker
//  All Rights Reserved
//
// Purpose:
//      This is a very special purpose mini_json.
//      I plan to use the mini_json format to communicate
//      between the cdr_pub and sub. The mini_json
//      conforms to the json spec, however, it does not
//      have any whitespace between the keys and values.
//

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <stddef.h>
#include "hiredis/hiredis.h"

// let's hope that this buffer is enough
#define BUFSIZE 8192
#define CHANNEL "cdr_pub"

char* strjson(char* s, char* k) {
    static char* j = NULL;
    static char* first;
    static char* last;
    if (s) {
        first = s;
        last = s+strlen(s);
    } else {
        if (j) {
            *j = '"';
        }
    }
    // reformat the key, add quotes and colon
    char key[100];
    memset(key, 0, sizeof(key));
    sprintf(key, "\"%s\":\"", k);
    printf("key: %s\n", key);

    // locate the key
    j = NULL;
    char *i = strstr(first, key);
    if (i) {
        printf("i: %s\n", i);
        i += strlen(key);
        printf("i+: %s\n", i);
        j = strstr(i, "\",");
        printf("j: %s\n", j);
        if (!j) {
            j = strstr(i, "\"}");
        }
        if (j) {
            *j = 0;
            printf("retval: %s\n", i);
            return i;            
        }        
    }
    // did not locate the key
    return 0;
}

int main(int argc, char** argv) {

    char* retval = NULL;
    char* json = malloc(BUFSIZE);
    assert(json);
    
    retval = NULL;
    memset(json, 0, BUFSIZE);
    strcpy(json,"{\"0\":\"zero\",\"1\":\"one\",\"2\":\"two\",}");
    
    retval = strjson(json,"0");
    assert(!strcmp(retval,"zero"));

    retval = strjson(NULL,"1");
    assert(!strcmp(retval,"one"));

    retval = strjson(NULL,"2");
    assert(!strcmp(retval,"two"));

    retval = strjson(NULL,"3");
    assert(retval == NULL);

    return 0;
}

// __END__
