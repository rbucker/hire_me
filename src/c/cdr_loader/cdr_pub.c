//  Copyright (c) 2012, Richard Bucker
//  All Rights Reserved
//
// This is the basic workflow:
//      create a local storage (hash)
//      store the command line options in the storage
//      read STDIN and convert the data to storage
//      convert the local storage to json or an sexp
//      pub the message to the local redis instance
//      if there is an error then send the msg to syslog
// NOTE:
//		there are plenty of opportunities for defensive
//		coding strategies but since this is an internal
//		program it's not going to matter.
// NOTE:
//      redis provides access to several varieties of
//      async APIs like libevent, libae etc. This is not
//      necessary here. It's strictly brute force.
// NOTE:
//      when creating the mini-JSON message, there should be
//      no whitespace between tokens.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include "hiredis/hiredis.h"

// let's hope that this buffer is enough
#define BUFSIZE 8192
#define CHANNEL "cdr_pub"

int main(int argc, char** argv) {
	char* sto = malloc(BUFSIZE);
	memset(sto, 0, BUFSIZE);

	strcat(sto, "{");
	printf("%d\n", argc);
	int a;
	for (a = 1; a<argc; a++) {
		sprintf(sto+strlen(sto), "\"%d\":\"%s\",", a, argv[a]);
	}

	char* s = malloc(BUFSIZE);
	memset(s, 0, BUFSIZE);
	while(fgets(s, BUFSIZE, stdin)) {
                if (strlen(s) == 0) {
                    break;
                }
		printf("%s\n", s);
		char* k = strtok(s, ":\n\r");
		char* v = strtok(s, ":\n\r");
                if (k && v) {
		    sprintf(sto+strlen(sto), "\"%s\":\"%s\",", k, v);
		    memset(s, 0, BUFSIZE);
                } else {
                    break;
                }
	}
	sto[strlen(sto)-1] = '}';
	printf("%s\n",sto);

    redisContext *c;
    redisReply *reply;
    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    c = redisConnectWithTimeout((char*)"127.0.0.1", 6379, timeout);
    if (c->err) {
    	syslog(LOG_WARNING, "%s", sto);
    	syslog(LOG_WARNING, "Redis Connection error: %s", c->errstr);
        exit(1);
    }

    reply = redisCommand(c,"PUBLISH %s %s", CHANNEL, sto);
    printf("PUBLISH: %lld\n", reply->integer);
    if (reply->integer == (long long int)0) {
    	syslog(LOG_WARNING, "%s", sto);
    } else {
    	syslog(LOG_INFO, "%s", sto);
    }
    freeReplyObject(reply);

	return 0;
}

// __END__
