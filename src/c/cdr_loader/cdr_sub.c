//  Copyright (c) 2012, Richard Bucker
//  All Rights Reserved
//
// This is the basic workflow:
//      open a connection to the redis DB
//      subscribe to the channel
//      wait for a message over the channel
//      convert the message to a CDR
//      construct the CDR filemname
//      open the file for appending
//      write the CDR
//      close the file
//      if there is an error then send the msg to syslog
// NOTE:
//		there are plenty of opportunities for defensive
//		coding strategies but since this is an internal
//		program it's not going to matter.
// NOTE:
//      redis provides access to several varieties of
//      async APIs like libevent, libae etc. This is not
//      necessary here. It's strictly brute force.

//#include <file.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <stddef.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "hiredis/hiredis.h"

// let's hope that this buffer is enough
#define HOSTNAME "switch.local"
#define PATH "/cdrs/"
#define BUFSIZE 8192
#define CHANNEL "cdr_pub"
char* foo[] = {"uid","2","3","4","5","6","7","8","9",
                "agi_channel","agi_type","agi_uniqueid","agi_callerid",
                "agi_calleridname","agi_dnid","agi_rdnis","agi_extension",
                "agi_accountcode","1","switch_ip","10","11", NULL}; 

char* cdrname(char* s) {
    // get the time and store it for use
    char st[50], ip[50];
    time_t tt;
    struct tm t;
    
    tt = time(&tt);
    gmtime_r(&tt, &t);
    strftime(st, 50, "%Y%m%d%H%M", &t);
    
    // make sure that switch.local is in the /etc/hosts
    struct hostent *h = gethostbyname(HOSTNAME);
    if (h && h->h_addr_list && h->h_addr_list[0]) {
        inet_ntop(AF_INET,h->h_addr_list[0], ip, 50);
        sprintf(s, "%s%s-%s-cdr.txt", PATH, st, ip);
        return s;
    }
    return NULL;
}

char* strjson(char* s, char* k) {
    static char* j = NULL;
    static char* first;
    static char* last;
    if (s) {
        first = s;
        last = s+strlen(s);
    } else {
        if (j) {
            *j = '"';
        }
    }
    // reformat the key, add quotes and colon
    char key[100];
    memset(key, 0, sizeof(key));
    sprintf(key, "\"%s\":\"", k);
    // locate the key
    j = NULL;
    char *i = strstr(first, key);
    if (i) {
        i += strlen(key);
        j = strstr(i, "\",");
        if (!j) {
            j = strstr(i, "\"}");
        }
        if (j) {
            *j = 0;
            return i;            
        }        
    }
    // did not locate the key
    return 0;
}

int main(int argc, char** argv) {
    syslog(LOG_WARNING, "cdr_sub started");
    redisContext *c;
    redisReply *reply;
    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    c = redisConnectWithTimeout((char*)"127.0.0.1", 6379, timeout);
    if (c->err) {
    	syslog(LOG_WARNING, "Redis Connection error: %s", c->errstr);
    	// something went wrong with the connection. Sleep for 5 then quit
    	sleep(5);
        exit(1);
    }

	char* cdr = malloc(BUFSIZE);
    memset(cdr, 0, BUFSIZE);
	char* sto = malloc(BUFSIZE);
	char* msg = malloc(BUFSIZE);
	char* filename = malloc(BUFSIZE);
    reply = redisCommand(c,"SUBSCRIBE %s", CHANNEL);
    printf("SUBSCRIBE: %lld\n", reply->integer);
    freeReplyObject(reply);
    printf("waiting\n");
    while(redisGetReply(c,(void**)&reply) == REDIS_OK) {
    	memset(msg, 0, BUFSIZE);
        if (!reply) {
            continue;
        }
        switch(reply->type) {
            case REDIS_REPLY_ARRAY:
                printf("got one (%d, %ld)\n", reply->type, reply->elements);
                int i;
                for (i=0; i<reply->elements; i++) {
                    printf("%d: %s\n", i, reply->element[i]->str);
                    if (reply->element[i]->str[0] == '{') {
                        printf("set (%d) %s\n", reply->element[i]->len, reply->element[i]->str);
                        strncpy(msg, reply->element[i]->str, reply->element[i]->len);
                        printf("cp\n");
                    }
                }
                break;
            case REDIS_REPLY_STRING:
                printf("set (%d) %s\n", reply->len, reply->str);
                strncpy(msg, reply->str, reply->len);
                printf("cp\n");
                break;
        }
        // consume message
        freeReplyObject(reply);
        printf("free\n");
        printf("msg: \%s\n", msg);
        
    	memset(sto, 0, BUFSIZE);
    	char *t = sto;
    	int i;
    	strjson(msg,"fred");
    	for (i=0; foo[i]; i++) {
    	    char *tok = strjson(NULL,foo[i]);
    	    if (tok) {
                strcat(t,tok);
            } else {
                strcat(t,"undefined");
            }
            strcat(t,"\t");
            //t += strlen(t);
    	}
    	printf("sto:%s\n", sto);
        
        if (cdrname(cdr)) {
        printf("filename: %s\n", cdr);
            FILE *f = fopen(cdr,"a");
            fprintf(f, "%s\n", sto);
            fclose(f);
        } else {
            // syslog
        	syslog(LOG_WARNING, "%s", sto);
        }
    }
    
    syslog(LOG_WARNING, "cdr_sub exiting; redisGetReply() not ok");
    sleep(5);
	return 0;
}

// __END__
