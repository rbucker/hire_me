#!/usr/bin/env perl
use Benchmark qw(:all) ;

"foaoooxbooycooxdooo" =~ /[abcd]o{2,3}(*PRUNE)(?{print "$&\n";$count++})(*F)/;
print $count . "\n";

$count = () = "foaoooxbooycooxdooo" =~ /[abcd]o{2,3}/g;
print $count . "\n";

($y,$m,$d) = "20120712" =~ /(\d{4})(\d{2})(\d{2})/;
print $y."\n";
print $m."\n";
print $d."\n";

($y,$m,$d) = unpack "a4a2a2", "20131231";
print $y."\n";
print $m."\n";
print $d."\n";


$count = 1000000;
cmpthese($count, {
    'unpack' => sub {($y,$m,$d) = unpack "a4a2a2", "20131231"},
    'regex' => sub {($y,$m,$d) = "20120712" =~ /(\d{4})(\d{2})(\d{2})/},
});
