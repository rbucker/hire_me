#!/usr/bin/env escript
%% -*- erlang -*-
%%! -smp enable -sname factorial -mnesia debug verbose
main([String]) ->
    try
        N = list_to_integer(String),
        F = itoa(N),
        io:format("itoa ~p = ~p~n", [String,F])
    catch
        _:_ ->
           usage()
    end;
main(_) ->
    usage().

usage() ->
    io:format("usage: itoa string~n"),
    halt(1).

itoa(0) -> [];
itoa(N) -> itoa(N div 10)++[(N rem 10)+$0].

