#!/usr/bin/env escript
%% -*- erlang -*-
%%! -smp enable -sname factorial -mnesia debug verbose
main([String]) ->
    try
        F = reverse(String),
        io:format("reverse ~p = ~p~n", [String,F])
    catch
        _:_ ->
           usage()
    end;
main(_) ->
    usage().

usage() ->
    io:format("usage: reverse string~n"),
    halt(1).

reverse([]) -> [];
reverse([L|R]) -> reverse(R)++[L].

