/*
The benefit of hindsight is that you can fix your mistakes.  In the 
original submission I should have dealt with negative numbers. That
has been addresed here.
*/

#include <stdio.h>
void itoab(char **s, int val){
    printf("DEBUG: %d\n", val);
    if (val<0) {
        **s = '-';
        (*s)++;
        itoab(s,abs(val));
        return ;
    }
    if (val == 0) {
        return;
    }
    itoab(s,val/10);
    *(*s) = '0'+(val%10);
    (*s)++;
    *(*s) = 0;
}

int main(int argc,char**argv) {
    int val = 32767;
    char s[15];
    char *p = s;
    itoab(&p,val);
    printf("%s\n",s);

    val = -32767;
    p = s;
    itoab(&p,val);
    printf("%s\n",s);
    return 0;
}
