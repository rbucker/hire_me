package main

import (
	"fmt"
)

type Klass struct {
	group []int
	Students []int
}

// PlayOne group at a time.
func (c Klass) PlayOne(v, pos int) (cnt int) {
	for i:=pos; i>=0; i-- {
		if c.Students[i] < v {
			continue
		}
		t := c.group[c.Students[i]] + 1
		if t > cnt {
			cnt = t
		}
	}
	return cnt
}

func (c Klass) play() (cnt int) {
	c.group = make([]int, len(c.Students)+1)
	for i, _ := range(c.Students) {
		v := c.Students[i]
		c.group[v] = c.PlayOne(v, i)
		if c.group[v] > cnt {
			cnt = c.group[v]
		}
	}
	return
}

func main() {
	fmt.Println(Klass{Students:[]int{1,2,3,4}}.play())
	fmt.Println(Klass{Students:[]int{4,3,2,1}}.play())
	fmt.Println(Klass{Students:[]int{3,2,4,1}}.play())
	fmt.Println(Klass{Students:[]int{6,1,2,3,4,5}}.play())
	fmt.Println(Klass{Students:[]int{5,1,4,3,2,6}}.play())
	fmt.Println(Klass{Students:[]int{1}}.play())
	fmt.Println(Klass{Students:[]int{2,1}}.play())
	fmt.Println(Klass{Students:[]int{1,2}}.play())
	fmt.Println(Klass{Students:[]int{13,2,10,3,1,12,8,20,5,16,19,6,15,14,11,17,7,4,9,18}}.play())
}

// __END__
