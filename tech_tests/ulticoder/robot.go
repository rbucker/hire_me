package main

import (
	"fmt"
)

type Path struct {
	X int
	Y int
}

type Track struct {
	Gates []int
	Paths []Path
}

// there are cycles
// 
func main() {
	fmt.Println("started")
	tracks := []Track{
		{[]int{10, 20, 30},[]Path{
			{2,1},
			{3,1},
			{2,4},
		}},
		{[]int{1,1,1,1},[]Path{
			{1,2},
			{2,3},
			{3,4},
			{1,4},
		}},
	}
	fmt.Println(tracks)
}

// __END__
