package main

import (
	"fmt"
	"math"
)

type Home struct {
	lat float64
	lng float64
}

type Town struct {
	homes []Home
}

type Dome struct {
	towns []Town
}

var data = []Dome{
	Dome{[]Town{Town{[]Home{Home{0, 0}, Home{0, 5}, Home{1, 2.5}}}}},
	Dome{[]Town{Town{[]Home{Home{1.25, -3.5}, Home{1.25, -7.5}, Home{5.25, -7.5}, Home{5.25, -3.5}}}}},
}

func main() {
	fmt.Println("started")
	fmt.Println(data)
	for d, dome := range data {
		dia := 0.0
		lat := 0.0
		lng := 0.0
		h := 0
		// find the center by averaging the points
		for _, town := range dome.towns {
			for _, home := range town.homes {
				h++
				lat += home.lat
				lng += home.lng
			}
		}
		lat /= float64(h)
		lng /= float64(h)
		// find the farthest point from the center
		for _, town := range dome.towns {
			for _, home := range town.homes {
				t := math.Sqrt(math.Abs(math.Pow(home.lat-lat, 2)+math.Pow(home.lng-lng, 2))) * 2.0
				if t > dia {
					dia = t
				}
			}
		}

		fmt.Printf("%d. Frost should put a dome %f feet in diameter at (%f, %f)\n", d, dia, lat, lng)
		_ = dome
	}
}

// __END__
