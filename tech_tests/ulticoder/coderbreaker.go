package main

import (
	"fmt"
)

type Message struct {
	A    int
	B    int
	C    int
	Text string
}

func rot(s string, n int) (retval string) {
        retval = s
	if len(s) <= 1 {
		return
	}
	for ; n>0; n-- {
		retval = retval[1:] + retval[0:1]
	}
	return 
}

func (m Message) Decode() (retval string) {
	retval = m.Text
	l := len(retval)
	for i:=0; i < l; {
		var prefix, suffix string
		if i > 0 {
			prefix = retval[:i]
		}
		if i+m.A < l {
			suffix = retval[i+m.A:]
		}
		if m.A + i >= l {
			retval = prefix + rot(retval[i:], m.B) + suffix
		} else {
			retval = prefix + rot(retval[i:i+m.A], m.B) + suffix
		}
		i += m.A + m.C
	}
	return
}

func main() {
	fmt.Println("Started")
	msgs := []Message{
		{3, 1, 1, "lHelbo rhote r.Wse hlouda wift onr atp iem"},
		{4, 2, 2, "foBerett aacngki D Er.vil"},
		{0, 0, 1555, "This string should also not change."},
		{0, 10, 5, "No change"},
		{1, 0, 12, "Same string here"},
		{4, 2, 2, "ttLileim Jmync eod tedhipls ano  tdeatfe DEvr.il."},
		{5, 5, 5, "Johnny you should flank Dr. Evil..."},
		{5, 5, 0, "Victory will be ours mauhahuahuhuhhzuae"},
		{27, 13, 0, " be a string      This will00 charactersthat will be 2And may or ma long with... bmissions to y not cause sualso includesfail.      It hitespace. 12 unnecessary wfghijklmnopqr34567890 abcdeabstuvwxyz"},
		{4, 2, 3, "reLom iumps dor lositme at, nscoecturet adisipici englitse, d deio usm todemp iorncidudint  lutabo eret dorole mnaag aluaiq. Uent im  madinivem nia qm,uisos ntruexd ercatitionll uamclao bor nisisit  u"},
		{50, 9, 1, " shorter.This string is going to be"},
		{17, 100000, 2,
		 " irure. Duis aute drehendolor in repertate vit in volupellum doit esse cillot nullre eu fugiaa xceptepariatur. Eurcat cu sint occaepiroidendatat non pt,lpa qu sunt in cui erunt officia desmollit anim"},
	}
	fmt.Println(msgs)
	for _, m := range(msgs) {
		fmt.Println(m.Decode())
	}
}

// __END__
