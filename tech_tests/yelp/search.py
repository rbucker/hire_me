"""
One of the interesting challenges with interview code samples is that it can be
treated like speed chess or even like the last 60 seconds of any basketball game.
You do not need to slaughter your aponent, just one move or basket ahead. And in the
case of an interview... just one CPU cycle better than the next person.

This code is terribly incomplete. It was a developing idea that was never
solid enough except to suggest that I had an outline of an idea. 

My recollection of the question is vague and I could produce the original
requirements doc but that wouldn't be any fun.

"""
import re
import itertools

DEBUG = True

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1))

def highlight_doc(doc, query):
    """
    Args:
        doc - String that is a document to be highlighted
        query - String that contains the search query
    Returns:
        The the most relevant snippet with the query terms highlighted.
    """
    # step 1: meta-up a regex (split the query into words and skip False)
    fields = filter(lambda x: x, re.split('\W+',query))
    if DEBUG:
        print "fields: ", fields
    permut = filter(lambda x: x, powerset(fields))
    permut.reverse()
    if DEBUG:
        # first position is hotest
        for a in permut:
            print 'permut:', a
    x = ''
    for a in permut:
        y = ''
        for b in a:
            y = "%s%s\s+" % (y, b)
        y = y[:-3]
        x = "%s(%s(%s)%s)|" % (x, '(\s|^)', y,'(\s|$)')
    x = x[:-1]

    # step 2: locate ALL of the instances (weighted by length)
    # step 3: heatmap to pick the best snippet
    # step 4: highlight the longest/hotest
    # step 5: return the result
    retval = x
    return retval


if "__main__" == __name__:
    doc = ''
    query = "foo bar\n"
    print "starting"
    print highlight_doc(doc, query)
    print "complete"