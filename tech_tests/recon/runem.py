#!/usr/bin/env python
import re

positions = {}
p = open("positions")
for r in p.xreadlines():
    (tick,val) = re.split(' *', r)
    positions[tick] = int(val)

e = open("executions")
for r in e.xreadlines():
    (tick,val) = re.split(' +', r)
    if not tick in positions:
        positions[tick] = 0
    positions[tick] = positions[tick] + int(val)


np = open("positions.new", "w+")
for k,v in positions.items():
    np.write("%s %d\n" % (k,v))

# __END__
