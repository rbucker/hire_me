#!/usr/bin/env perl

use strict;
use warnings;


my %positions;

sub read_ticks {
  my ($input_filename) = @_;
  open my $p, "<", $input_filename or die "Could not open '$input_filename' - $!";
  while(<$p>) {
    chomp;
    my ($tick,$val) = split / +/, $_;
    $positions{$tick} += $val;
  }
}

read_ticks("positions");
read_ticks("executions");

my $output_filename = 'positions.new';
open my $np, ">", $output_filename or die "Could not open '$output_filename' - $!";
foreach (keys %positions) {
  printf {$np} "$_ $positions{$_}\n";
}

__END__
