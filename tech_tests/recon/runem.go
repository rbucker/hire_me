package main

import (
        "fmt"
        "os"
        "bufio"
        "regexp"
        "strconv"
        )

var positions = map[string] int {}

func main() {
  re, _ := regexp.Compile(`([A-Z]*) +(-?\d*)`)
  file, _ := os.Open("positions")
  p := bufio.NewReader(file)
  for line, _, err := p.ReadLine(); err == nil; line, _, err = p.ReadLine() {
    m := re.FindStringSubmatch(string(line)) 
    positions[m[1]], _ =  strconv.Atoi(m[2])
  }
  file, _ = os.Open("executions")
  p = bufio.NewReader(file)
  for line, _, err := p.ReadLine(); err == nil; line, _, err = p.ReadLine() {
    m := re.FindStringSubmatch(string(line)) 
    val, _ :=  strconv.Atoi(m[2])
    positions[m[1]] = positions[m[1]] + val
  }
  file, _ = os.Create("positions.new")
  for k,v := range positions {
    fmt.Fprintf(file, "%s %d\n",k,v)
  }

}
