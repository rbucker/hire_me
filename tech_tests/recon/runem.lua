#!/usr/bin/env lua

local positions = {}

local p = io.open("positions")
for line in p:lines() do
  local tick, val = string.match(line, "(%w+) +(-?%w+)")
  positions[tick] = val
end

local e = io.open("executions")
for line in e:lines() do
  local tick, val = string.match(line, "(%w+) +(-?%w+)")
  if positions[tick] == nil then
    positions[tick] = 0
  end
  positions[tick] = positions[tick] + val
end

local np = io.open("positions.new", "w+")
for k,v in pairs(positions) do
  np:write(string.format("%s %d\n", k, v))
end

-- __END__
