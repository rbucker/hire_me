#!/usr/bin/env ruby

positions = {}

File.open("positions", "r") do |file|
  while line = file.gets
    tick, val = line.split
    positions[tick] = Integer(val).to_i
  end
end

File.open("executions", "r") do |file|
  while line = file.gets
    tick, val = line.split
    if not positions[tick] then
      positions[tick] = 0
    end
    positions[tick] += Integer(val).to_i
  end
end

File.open("positions.new", "w+") do |file|
  positions.keys.each do |key|
    file.puts("#{key} #{positions[key]}")
  end
end

# __END__
