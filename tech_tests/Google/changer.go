package main

import (
	"fmt"
	"strings"
)

type Net struct {
	net map[string][]string
}

// the code does not insure that the src and tgt words are in the dict and the same length
func main() {
	fmt.Println("started")
	dict := []string{"fred", "barney", "axe", "fig", "fog", "dog", "pat", "pit", "dig", "bam", "ping", "tip", "dag"}
	net := Net{findadj(3, dict)}
	fmt.Println(net)
	fmt.Println(net.findpath("", "fig", "dog"))
	fmt.Println(strings.Join(net.findallpath("", "fig", "dog"), ", "))
}

// generate a complete list of all of the paths
// NOTE: I did not check the empty set or validate the start/end strings are the same length.. etc
func (net Net) findallpath(acc, start, end string) (retval []string) {
	if start == end {
		return append([]string{}, acc)
	}
	if acc == "" {
		acc = start
	}
	for _, k := range net.net[start] {
		if strings.Contains(acc, k) {
			continue
		}
		if sol := net.findallpath(acc+" -> "+k, k, end); sol != nil {
			retval = append(retval, sol...)
		}
	}
	return retval
}

// find a path from the start to the end string through adjacent strings
// NOTE: I did not check the empty set
func (net Net) findpath(acc, start, end string) string {
	if start == end {
		return acc
	}
	if acc == "" {
		acc = start
	}
	for _, k := range net.net[start] {
		if strings.Contains(acc, k) {
			continue
		}
		if sol := net.findpath(acc+" -> "+k, k, end); sol != "" {
			return sol
		}
	}
	return ""
}

// from the dictionary compute the complete list of adjacent words
func findadj(ilen int, dict []string) (retval map[string][]string) {
	retval = make(map[string][]string)
	tmp := make([]string, 0)
	for _, k := range dict {
		if len(k) == ilen {
			tmp = append(tmp, k)
		}
	}

	for i, k1 := range tmp {
		for _, k2 := range tmp[i+1:] {
			if isadj(k1, k2) {
				retval[k1] = append(retval[k1], k2)
				retval[k2] = append(retval[k2], k1)
			}
		}
	}
	return
}

// determine if two strings are adjacent meaning that they differ
// by one char with the same length
func isadj(a, b string) bool {
	delta := 0
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			delta++
			if delta > 1 {
				return false
			}
		}
	}
	return (delta == 1)
}
