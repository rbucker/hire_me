#!/usr/bin/env python
import random
def monte_hall(change):
    car   = random.randint(1,3)
    guess = random.randint(1,3)
    if change:
        return guess != car
    return guess == car
def monte_run(change, trials):
    wins = reduce(lambda acc, trial: acc+monte_hall(change), xrange(1, trials))
    print "trials (%d), wins (%d) ratio (%f)" % (trials, wins, (wins / (trials*1.0)))
if __name__ == "__main__":
    trials = 10000
    monte_run(False, trials)
    monte_run(True, trials)
