#!/usr/bin/env perl
use List::Util qw(reduce);

sub monte_hall {
    my ($change) = @_;
    my $car   = int(rand(3));
    my $guess = int(rand(3));
    return $guess != $car if $change;
    return $guess == $car;
}

sub monte_run {
    my ($change, $trials) = @_;
    my $wins = reduce { $a + monte_hall($change) } 0, 1 .. $trials;
    printf("trials (%d), wins (%d) ratio (%f)\n", $trials, $wins, ($wins / ($trials*1.0)));
}

my $trials = 10000;
monte_run(0, $trials);
monte_run(1, $trials);

__END__
