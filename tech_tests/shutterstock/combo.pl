#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
use Bit::Vector::Overload;
use List::Util qw(first max maxstr min minstr reduce shuffle sum);
use List::MoreUtils qw( pairwise );

my $target_sum    = 10;
my %data    = (apple=>3, pear=>2, pineapple=>9, strawberry=>1, 
               kiwi=>-5, lime=>15, lemon=>5, orange=>-1, banana=>5);
my @keys    = keys %data;
my $n       = @keys;

my $vector  = Bit::Vector::Overload->new($n);
$vector->Configuration("output=bin");
$vector->Fill();

print Dumper(\%data);
$Data::Dumper::Indent = 0;
my $carry = 0;
while( ! $carry ) {
  my @bits = split(//, "$vector");
  my @pairs= pairwise { [$a,$b] } @keys, @bits;

  my $kset = (reduce { push($a,@$b[0]) if(int(@$b[1])); $a} [], @pairs)[0]; 
  my $sum  = reduce { $a + $data{$b} } 0, @$kset;
  
  if( $sum == $target_sum ) {
    print $vector->to_Hex().": ".Dumper($kset) ."\n";
  }
  $carry = $vector->decrement();
}

# __END__
