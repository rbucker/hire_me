"""
Copyright (c) 2012, Richard Bucker

All of my xmlrpc test cases are going to go here
"""
__author__ = 'rbucker'

from spotify_test.config import get_config


def test_json():
    """
    testing the simple echo transaction
    """
    fqbn = 'xmlrpc_connect'
    from xmlrpclib import Server
    server = Server("http://%s" % (get_config().get(fqbn)))
    print server.echo()

# __END__