"""
Copyright (c) 2012, Richard Bucker

it would be nice if the JSON and XML versions could be merged into one lib.
That, however, is not currently possible because they use the same base URL.
(/RPC2). So the only way to differentiate them is through port number.

**the brokerless implementation falls apart when there is more than one webserver.
"""
__author__ = 'rbucker'
import uuid
import logging
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler

import tornado
import tornado.options
from tornadorpc.xml import XMLRPCHandler
import tornado.ioloop
import tornado.web
import zmq.eventloop

from datastore import DataStore, get_datastore
from bus import get_push_bus, get_sub_bus
from helpers import whoami
from config import get_config

iama = 'apixmlrpc'
logging.getLogger().addHandler(RotatingFileHandler('/var/log/%s.log'%(iama), mode='a', maxBytes=500000, backupCount=10))

class Handler(XMLRPCHandler):
    def initialize(self, datastore):
        self.ds = datastore
        super(Handler, self).initialize()

    #@async
    def echo(self):
        #return iama
        u = str(uuid.uuid1())
        sub.subscribe(u)
        push_echo.reply_to(u)
        push_echo.send("Hello, %s - %s" % (whoami(), u))
        m = sub.recv()[0]
        sub.unsubscribe(u)
        return m


sub = None
push_echo = None

application = tornado.web.Application([
    # the order of these URIs is important so make sure that most
    # unique is before the generic and the empty is the last
    (r'/RPC2',  Handler, dict(datastore=DataStore())),
    (r'/',      Handler, dict(datastore=DataStore())),
    ])

def main():
    """
    this is the main entrypoint for tornadoweb. we should check the
    command line for things like sockets, threads, etc.
    """
    global sub
    sub = get_sub_bus(None)
    global push_echo
    push_echo = get_push_bus(None, 'echo')
    zmq.eventloop.ioloop.install()
    [host, port] = get_config().get('xmlrpc_bind').split(':')
    if host == '*':
        host = '0.0.0.0'
    application.listen(port, address=host)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    tornado.options.parse_command_line()
    main()

# __END__