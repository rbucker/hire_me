"""
Copyright (c) 2012, Richard Bucker

"""
# Example package with a console entry point
import apirest

def main():
    return apirest.main()

# __END__