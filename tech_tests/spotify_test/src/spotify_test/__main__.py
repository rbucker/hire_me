"""
Copyright (c) 2012, Richard Bucker

"""
## {{{ http://code.activestate.com/recipes/577088/ (r1)
#!/usr/bin/env python
import apirest

if __name__ == '__main__':
    apirest.main()

# __END__