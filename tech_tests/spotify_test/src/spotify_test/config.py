"""
Copyright (c) 2012, Richard Bucker

This is the config file. This should be replaced with a datastore later.
"""
__author__ = 'rbucker'

__config__ = {
    'worker_bind_connect':      'bind',
    'mover_bind':               '*:9003',
    'mover_connect':            'localhost:9003',
    'register_bind':            '*:9004',
    'register_connect':         'localhost:9004',

    'echo':                     '["tcp://127.0.0.1:9005",]',
    'response':                 '["tcp://127.0.0.1:9015",]',
    #'echo':                     '["tcp://127.0.0.1:9005","tcp://127.0.0.1:9006","tcp://127.0.0.1:9007"]',
    #'response':                 '["tcp://127.0.0.1:9015","tcp://127.0.0.1:9016","tcp://127.0.0.1:9017"]',

    'jsonrpc_bind':             '*:8887',
    'jsonrpc_connect':          'localhost:8887',
    'xmlrpc_bind':              '*:8886',
    'xmlrpc_connect':           'localhost:8886',
    'restapi_bind':             '*:8888',
    'restapi_connect':          'localhost:8888',
}

config = None
def get_config():
    global config
    if not config:
        config = Config(__config__)
    return config

class Config(object):
    """
    This is a standard singleton hash container. (aka dictionary). The
    intent is that it would be replaced with a global KV store of some
    kind. It could even be replaced with the datastore used in this app.
    """
    def __init__(self, kvconfig):
        """
        capture the config hash.
        """
        self.kvconfig = kvconfig

    def get(self, key):
        """
        return a value based on the key.
        """
        return self.kvconfig.get(key)

    def set(self, key, value):
        """
        set or replace a particular key with the value provided.
        """
        self.kvconfig[key] = value

# __END__