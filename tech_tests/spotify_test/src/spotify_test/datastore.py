"""
Copyright (c) 2012, Richard Bucker

"""
__author__ = 'rbucker'

datastore = None
def get_datastore():
    global datastore
    if not datastore:
        datastore = DataStore()
    return datastore

class DataStore(object):
    """

    """
    def hash(self, key):
        """
        There are 71 fixed shards. They are distributed across each node
        in the cluster. When a new node is added the new node is considered
        semi-suspended. Writes are sent to the new node and reads are sent
        to the new then the old node. In the meantime an event is sent to
        a daemon which will start moving and registering shards from the
        old server to the new server.
        """
        pass

# __END__