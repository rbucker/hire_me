"""
Copyright (c) 2012, Richard Bucker

**the brokerless implementation falls apart when there is more than one webserver.
"""
from tornadorpc.base import async

__author__ = 'rbucker'
import uuid

import tornado.ioloop
import tornado.web
from tornado.web import asynchronous
import zmq.eventloop

from datastore import DataStore, get_datastore
from bus import get_push_bus, get_sub_bus
from helpers import whoami



class RegisterHandler(tornado.web.RequestHandler):
    """
    Storage nodes need to be able to communicate with each other and
    at least have a list of peers in order to hash as part of the
    sharding process.

    TODO: define the URLs
    """
    def initialize(self, datastore):
        self.ds = datastore
        super(RegisterHandler, self).initialize()

    def put(self, *args, **kwargs):
        """
        a peer datastore note is registering itself with the cluster
        """
        self.write("Hello, world")

    def get(self, *args, **kwargs):
        """
        get the list of registered nodes in the cluster and return it
        """
        self.write("Hello, world")

    def delete(self, *args, **kwargs):
        """
        remove a node from the cluster
        """
        self.write("Hello, world")


class APIHandler(tornado.web.RequestHandler):
    """
    GET/PUT/DELETE key, value in the datastore

    TODO: define the URLs
    """
    def initialize(self, datastore):
        self.ds = datastore
        super(APIHandler, self).initialize()

    def put(self, *args, **kwargs):
        """
        PUT the record in the datastore
        """
        self.write("Hello, world")

    def get(self, *args, **kwargs):
        """
        GET the value from the datastore
        """
        self.write("Hello, world")

    def delete(self, *args, **kwargs):
        """
        DELETE the key/value in the datastore
        """
        self.write("Hello, world")


class EchoHandler(tornado.web.RequestHandler):
    """
    send the echo response, ignore everything else

    TODO: add a wildcard to the URLs in the handler list and ignore
    """
    def initialize(self, datastore):
        self.ds = datastore
        super(EchoHandler, self).initialize()

    @asynchronous
    def put(self, *args, **kwargs):
        """
        send the echo response, ignore everything else
        """
        u = str(uuid.uuid1())
        sub.subscribe(u)
        push_echo.reply_to(u)
        push_echo.send("Hello, %s - %s" % (whoami(), u))
        m = sub.recv()[0]
        self.write(m)
        sub.unsubscribe(u)

    @asynchronous
    def get(self, *args, **kwargs):
        """
        send the echo response, ignore everything else
        """
        u = str(uuid.uuid1())
        sub.subscribe(u)
        push_echo.reply_to(u)
        push_echo.send("Hello, %s - %s" % (whoami(), u))
        m = sub.recv()[0]
        self.write(m)
        sub.unsubscribe(u)

    @asynchronous
    def delete(self, *args, **kwargs):
        """
        send the echo response, ignore everything else
        """
        u = str(uuid.uuid1())
        sub.subscribe(u)
        push_echo.reply_to(u)
        push_echo.send("Hello, %s - %s" % (whoami(), u))
        m = sub.recv()[0]
        self.write(m)
        sub.unsubscribe(u)


# the sub and sub_echo global variables WORK! One would think that since
# this application is a single threaded application and that there were
# multiple transactions in flight and still asynchronous in design that
# if the system were bombarded with traffic that more than one transaction
# would effect the state of the global socket instance.
#
# that's what you would think. but you'd be wrong.
#
# Only one thread enters into this global space at a time. Therefore
# by extension, only one transaction is in the Handler space at a time.
# All the socket magic happens in the IOLoop.
#
sub = None
push_echo = None

application = tornado.web.Application([
    (r"/echo", EchoHandler, dict(datastore=DataStore())),
    (r"/echo/", EchoHandler, dict(datastore=DataStore())),
    (r"/api", APIHandler, dict(datastore=DataStore())),
    (r"/api/", APIHandler, dict(datastore=DataStore())),
    (r"/register", RegisterHandler, dict(datastore=DataStore())),
    (r"/register/", RegisterHandler, dict(datastore=DataStore())),
])

def main():
    """
    this is the main entrypoint for tornadoweb. we should check the
    command line for things like sockets, threads, etc.
    """
    global sub
    sub = get_sub_bus(None)
    global push_echo
    push_echo = get_push_bus(None, 'echo')
    zmq.eventloop.ioloop.install()
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

# __END__