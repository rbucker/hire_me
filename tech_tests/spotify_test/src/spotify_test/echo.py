"""
Copyright (c) 2012, Richard Bucker

this is a test worker. It's sole purpose is to pull
messages from the pusher and echo them back. It is the
basis for the real worker.

There is no reason to trap exceptions and such here.
"""
__author__ = 'rbucker'
import tornado.options
from tornado.options import define, options

from bus import get_pull_bus, get_pub_bus

if __name__ == '__main__':
    # I thought about using sys and os in order to get the "bus_name" from
    # python, while it might be reliable it's better to be explicit
    define("echo_type", default='r', help="this is my node type(r, j, x)", type=str)
    tornado.options.parse_command_line()

    # connect to all of the web servers for pulling
    p = get_pull_bus(bus_name='echo')
    # connect to all of the web server for responding
    r = get_pub_bus(None, bus_name='response')

    while True:
        [channel, message] = p.recv_multipart()
        if channel:
            # we always expect a channel to be present. But it's no
            # big deal if it's missing
            r.reply_to(channel)
            print message
            r.send(message)

# __END__