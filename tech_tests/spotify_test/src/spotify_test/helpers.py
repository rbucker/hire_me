"""
Copyright (c) 2012, Richard Bucker

"""
__author__ = 'rbucker'
import os
import inspect


def whoami():
    return os.path.basename(inspect.stack()[1][1])+":"+inspect.stack()[1][3]

def whosdaddy():
    return os.path.basename(inspect.stack()[2][1])+":"+inspect.stack()[2][3]


# __END__