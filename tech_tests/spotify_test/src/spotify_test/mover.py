"""
Copyright (c) 2012, Richard Bucker

this is a queue worker. It's sole purpose is to pull
messages from the pusher and work the command to completion.

There is no reason to trap exceptions and such here. This
worker is going to be started by daemontools. restarts
are going to be logged and monitored. Let the exceptions
fall through.
"""
__author__ = 'rbucker'
import tornado.options
from tornado.options import define, options

from bus import get_pull_bus, get_pub_bus

if __name__ == '__main__':
    # I thought about using sys and os in order to get the "bus_name" from
    # python, while it might be reliable it's better to be explicit
    define("echo_type", default='r', help="this is my node type(r, j, x)", type=str)
    tornado.options.parse_command_line()
    p = get_pull_bus(bus_name='echo'+options.echo_type)
    while True:
        [channel, message] = p.recv_multipart()
        if channel:
            # we always expect a channel to be present. But it's no
            # big deal if it's missing
            bus_name = 'response' + options.echo_type
            r = get_pub_bus(channel, bus_name=bus_name)
            print message
            r.send(message)
            r.close()

# __END__