"""
Copyright (c) 2012, Richard Bucker

I hate to say that this is a bit tricky but it is ina way. The publisher and
subscriber are singletons per instance of the tornadoweb and need to be
singletons on the entire box because more than one process ot thread
cannot bind to the same network adapter and port. The other issue is that
there are multiple buses.


- There is one server instance per bus type on the webserver
- There is one connection per worker instance per bus from the worker to the
  webserver.
- there is one pub/sub server and connection per transaction with a response.

NOTE: I've been having trouble with reliability depending on whether the handler
binds or connects. Looking at the Mongrel2 code I see that the Handler, in
Mongrel2, binds.

Server                                                        Worker
-----------                                                   ----------
O ---> PUSH                     --->                          PULL --->|
                                                                       |
O <--- SUB                      <---                          PUB  <---|


The default config for left_right:
             'left_right':               'right'
"""
__author__ = 'rbucker'
import time

from zmq.eventloop import ioloop
import zmq
import zmq.eventloop.zmqstream

from config import get_config

context = zmq.Context()
sockets = {}


def zmq_bind(connection_context, bus_name):
    """
    get a bus server instance for this pusher
    """
    socket = context.socket(connection_context)
    addrs = get_config().get(bus_name+'_bind') or get_config().get(bus_name)
    c = eval(addrs)
    # get the first
    for a in c:
        try:
            print "ZMQ binding to (%s)..." % a
            socket.bind(a)
            socket.setsockopt(zmq.LINGER, 0)
            print "ZMQ bound to (%s)" % a
            break
        except zmq.ZMQError, msg:
            print msg
            continue
        # we only send fqbn on client connect, not server bind
    return socket


def zmq_connect(connection_context, bus_name):
    """
    get a bus client connection for this puller

    TODO: since zmq supports multiple connections to multiple servers
    this code should be enhanced. ie, one worker connecting to more
    than one webserver
    """
    socket = context.socket(connection_context)
    addrs = get_config().get(bus_name+'_connect') or get_config().get(bus_name)
    c = eval(addrs)
    for a in c:
        print "ZMQ connecting to (%s)..." % (a)
        socket.connect(a)
        socket.setsockopt(zmq.LINGER, 0)
        print "ZMQ connected to (%s)" % (a)

    return socket


def zmq_stream(zmq_socket):
    """
    """
    return zmq.eventloop.zmqstream.ZMQStream(zmq_socket)


def get_bus(ctx, bus_name, bind_or_connect, callback):
    """
    """
    socket = None
    if bind_or_connect:
        socket = zmq_bind(ctx, bus_name)
    else:
        socket = zmq_connect(ctx, bus_name)
    if callback:
        socket = zmq_stream(socket)
    return socket


def get_push_bus(channel, bus_name=None, callback=None):
    """
    get a bus server instance for this pusher
    """
    ctx = zmq.PUSH
    bind_or_connect = get_config().get('worker_bind_connect') != 'bind'
    socket = get_bus(ctx, bus_name, bind_or_connect, callback)
    return Bus(channel, sock_type='pusher', socket=socket, callback=callback)


def get_pull_bus(bus_name=None, callback=None):
    """
    get a bus client connection for this puller

    TODO: since zmq supports multiple connections to multiple servers
    this code should be enhanced. ie, one worker connecting to more
    than one webserver
    """
    ctx = zmq.PULL
    bind_or_connect = get_config().get('worker_bind_connect') == 'bind'
    socket = get_bus(ctx, bus_name, bind_or_connect, callback)
    return Bus(None, sock_type='puller', socket=socket, callback=callback)


def get_pub_bus(channel, bus_name='response', callback=None):
    """
    get a bus client connection for this publisher

    TODO: since zmq supports multiple connections to multiple servers
    this code should be enhanced. ie, one worker connecting to more
    than one webserver
    """
    ctx = zmq.PUB
    bind_or_connect = get_config().get('worker_bind_connect') == 'bind'
    socket = get_bus(ctx, bus_name, bind_or_connect, callback)
    return Bus(channel, sock_type='publisher', socket=socket, callback=callback)


def get_sub_bus(channel, bus_name='response', callback=None):
    """
    get a bus server instance for this subscription
    """
    # we only send fqbn on client connect, not server bind
    ctx = zmq.SUB
    bind_or_connect = get_config().get('worker_bind_connect') != 'bind'
    socket = get_bus(ctx, bus_name, bind_or_connect, callback)
    return Bus(channel, sock_type='subscriber', socket=socket, callback=callback)


class Bus(object):
    """
    This class will implement a ZMQ bus between the tornado client
    and the datastore and mover workers. The webclient will push
    transactions onto the queue, the workers will pull transactions
    from the queue. The response from the worker to the client will
    be a pub/sub over a private channel constructed from a uuid.
    (something akin to Mongrel2)

    I'm not doing anything with ZMQ_IDENTITY. While this is good for
    longer and more persistent connections in this case it's not
    valuable. If either side of the transaction is terminated by
    a failure of a worker or tornado client then the transaction
    will be lost anyway. The only counter to this would be if the
    transactions in flight were more persistent and only the
    message_id were passed around.
    """
    def __init__(self, channel, sock_type=None, socket=None, fqbn=None, callback=None, timeout_seconds=5):
        """
        publisher, subscriber, pusher, puller are all ZMQ sockets. This code could have
        been written any number of ways, including a single socket instance and a
        socket instance type. The presumption is that there is only one socket instance
        open for each leg of the transaction. In this case, however, the push/pull
        is a single socket and the response in the pub/sub is a second socket. So
        a single socket instance is not useful.

        There is nothing wrong with a special purpose bus.

        TODO: this is here just to see if you read all of my comments
        """
        self.channel = channel
        self.sock_type = sock_type
        self.socket = socket
        self.fqbn = fqbn
        self.timeout_seconds = timeout_seconds
        if channel and sock_type=='subscriber':
            self.socket.setsockopt (zmq.SUBSCRIBE, self.channel)
        self.set_callback(callback)


    def clear_callback(self):
        self.callback = None
        if self.socket and isinstance(self.socket, zmq.eventloop.zmqstream.ZMQStream):
            self.socket.on_recv(None)
            self.socket.on_send(None)
            #self.socket.on_err(self._socket_err)
            self.socket.on_send(None)
            self.socket.set_close_callback(None)
            if self.timeout_handle:
                ioloop.IOLoop.instance().remove_timeout(self.timeout_handle)
                self.timeout_handle = None

    def set_callback(self, callback):
        if callback and self.socket and isinstance(self.socket, zmq.eventloop.zmqstream.ZMQStream):
            self.callback = callback
            self.socket.on_recv(self._received)
            self.socket.on_send(self._socket_sent)
            #self.socket.on_err(self._socket_err)
            self.socket.on_send(self._socket_sent)
            self.socket.set_close_callback(self._socket_closed)
            self.timeout_handle = ioloop.IOLoop.instance().add_timeout(
                                    time.time()+self.timeout_seconds,
                                    self._socket_timeout
                                   )
        else:
            self.clear_callback()

    def _stop_timeout(self):
        if self.timeout_handle:
            ioloop.IOLoop.instance().remove_timeout(self.timeout_handle)
            self.timeout_handle = None

    def _received(self, msg):
        """
        we have received a response
        """
        print "-> %s" % msg
        if self.callback:
            # timing is everything; the callback is about to be cleared.
            lcallback = self.callback
            self.clear_callback()
            lcallback(msg)


    def _socket_sent(self, msg, result):
        """
        the message was sent
        """
        #self.clear_callback()
        pass

    #def _socket_error(self, ):
    #    """
    #    there was an error with the transaction
    #    """
    #    pass

    def _socket_timeout(self):
        """
        there was a transaction timeout
        """
        self.clear_callback()

    def _socket_closed(self):
        """
        the socket was closed
        """
        self.clear_callback()

    def close(self):
        """
        close whatever was open.
        """
        if self.sock_type=='publisher' or self.sock_type=='puller':
            self.socket.close()
            self.socket = None
        if self.sock_type=='subscriber':
            self.socket.setsockopt (zmq.UNSUBSCRIBE, self.channel)
        if self.fqbn:
            global sockets
            del sockets[self.fqbn]
        self.clear_callback()

    def unsubscribe(self, channel):
        if self.sock_type=='subscriber':
            self.socket.setsockopt (zmq.UNSUBSCRIBE, channel)

    def subscribe(self, channel):
        if self.sock_type=='subscriber':
            self.socket.setsockopt (zmq.SUBSCRIBE, channel)

    def reply_to(self, channel):
        if self.sock_type=='pusher' or self.sock_type=='publisher':
            self.channel = channel

    def send(self, message):
        """
        send a single message by wrapping it up in a default multipart. ZMQ
        is optimized for this sort of behavior.
        """
        self.send_multipart([message,])

    def send_multipart(self, messages):
        """
        xmit a message to the remote worker. encapsulate the message, NOBLOCK will
        force zmq to throw an exception if the worker is not connected. This is
        better than waiting for the broker to arrive as transactions back up in
        the queue.
        """
        new_messages = [self.channel] + messages
        self.socket.send_multipart(new_messages)

    def recv(self):
        """
        In any internal message stack the first item in the multipart message
        is always going to be the message_id aka the pub/sub channel.
        """
        return self.recv_multipart()[1:]

    def recv_multipart(self):
        """
        receive a message from the xmitter
        """
        return self.socket.recv_multipart()

# __END__
