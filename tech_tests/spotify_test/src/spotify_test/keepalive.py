"""
Copyright (c) 2012, Richard Bucker

This is the keepalive worker.  This daemon will implement a tight and
simple ZMQ.poll() loop. With each timeout it is going to check to see whether
all of the cluster peers are present and functioning. If any are missing
then it will tell the others that they are missing. The polling means
that it can receive messages from the client. (currently undefined)

It sill also register/re-register this node with the peers.

The resharding is handled by the mover worker.
"""
__author__ = 'rbucker'




# __END__