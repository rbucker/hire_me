This file requires editing
==========================

Note to the author: Please add something informative to this README *before*
releasing your software, as `a little documentation goes a long way`_.  Both
README.rst (this file) and NEWS.txt (release notes) will be included in your
package metadata which gets displayed in the PyPI page for your project.

You can take a look at the README.txt of other projects, such as repoze.bfg
(http://bfg.repoze.org/trac/browser/trunk/README.txt) for some ideas.

.. _`a little documentation goes a long way`: http://www.martinaspeli.net/articles/a-little-documentation-goes-a-long-way

Credits
-------
Copyright (c) 2012, Richard Bucker


- `Distribute`_
- `Buildout`_
- `modern-package-template`_

.. _Buildout: http://www.buildout.org/
.. _Distribute: http://pypi.python.org/pypi/distribute
.. _`modern-package-template`: http://pypi.python.org/pypi/modern-package-template


Spotify recruitment assignment - key–value storage
==================================================

Introduction
------------
A test is the best way to get an understanding of the knowledge and experience
of the candidate (you). We will read and test the code you submit to us, but
always keep in mind that we want to first and foremost get an understanding
of your skillset.

Background
----------
The Spotify backend is built out of loosely coupled services that are accessed
over HTTP. You are to build a distributed key–value storage that could be
plugged into said backend.

Deployment
----------
The service should run on a modern version of Linux, and should be easy to
run. Please provide a README file on how to set up and run the system.

Installation
------------
- download the code to a folder on your computer
- If you have virtualenv installed that would be best
- install the dependencies as needed (listed in this doc)
- then install this code
    python ./setup.py install

How To Run
----------
- open two console windows
- in the first window launch the worker
    python -m spotify_test.echo
- in the second window launch the webserver
    python -m spotify_test
- if you want to launch the json or xml server you'll need a new window


How To Test
-----------
There are two tings you can do now. Launch the page from your browser:
   http://localhost:8888/echo/

or you can run the test program siege:
   siege -c 5 http://localhost:8888/echo/

** apache bench (ab) works to some degree, however, ab and tornadoweb have some
flaky issues that I've never addressed.

Languages
---------
Write the service in Python (2.6 or 2.7).

** development was performed on a Mac OSX Lion. Before launch I might try it on
ubuntu... but there is no compelling reason at the moment.

Scope
-----
A minimal system would support:

- Predefined network topology
- Assume nodes are always up (data persistence or failover not needed)
- At least two nodes dividing responsibilities between themselves
- HTTP based GET/PUT/DELETE of data

The notion of dividing responsibilities can either be put on the client
driver running on the client or it can be on the server. On the server,
then each server is or will act as a proxy for the final destination.
There are some good and bad reasons for both decisions. The decision
to move the client/server routing to/from the client/server can only
be made after watching some realtime data in order to keep the network
chattiness to a minimum. All thinks being equal I prefer the server
as a proxy to the hash rather than the client.

We would like to see an implementation of at least one the following areas:

- Data persistence
 - my DataStore class uses redis

- Scalability (dynamic rebalancing as nodes are added and removed)
 - documented in this document, the mover.py file would implement the request

- Multiple levels of redundancy (how many nodes store a key-value pair)
 - configured number of hash+1 nodes to write the data
 - need to make sure that the +1 is not on the same node.

- Handling failures (node crashes or looses contact with the rest of the network)
 - this is handled by the previous 2 notes

- Other external interfaces
 - jsonrpc
 - xmlrpc

- Testing
 - echo
 - ab
 - seige


Consider how to address all the issues listed above and others you may
think are needed in a distributed key-value storage. These will be discussed
in the interview.

Please try to keep a focus on a simple and clean implementation. A smaller
implementation demonstrating good coding and design skills together with
thoughtful consideration on how the system would be build out in the future
is ideal.

Module Dependencies
-------------------
TornadoWeb -
requests -
pystache -
pyzmq -
tornadorpc -
jsonrpclib -
nose -

siege - google benchmarking tool

Design
------
TornadoWeb is event driven and not threaded. Therefore, in order to get some
good performance the frontend is going to get the request from the user
and return a response as quickly as possible. If the task is a long running
task then the event will be queued in order to be worked on by a ZeroMQ
worker application. (ZeroMQ integrates nicely with IOLoop used in TornadoWeb).

There are going to be 71 predefined shards. They will initially exist on the
master. The master holds no special power other than it was the first node
started in the cluster. When an orphan node registers with any of the current
nodes it will be given a list of the current nodes. It must register with all
of the existing nodes.

(only one node can register and acquire shards at a time)

Just because the node exists does not mean it has any data. The next step is
to acquire shards from it's current occupant. This is a mixed bag of tricks.
The question is, "which shards are you going to move?". The ones with the
most data, least data, distributed poorly, unbalanced? There are a number
of strategies. The hope is that the data is evenly distributed but that's
not always going to be the case.

The new node will now iterate over the list of nodes in the cluster and do
the following:

- how many shards do you have?
- I have 0, if you have more than one, please send me one.
- I have more than one less than you. Please send me one!
- I have almost as many as you. So I do not need any more.
- When there are 71 nodes then each should have one.
- When node 72 arrives either say no thanks you cannot join or maybe join but empty.

Keep going around and around until a single pass is clean.

Limits
------
This code does not care about ACID. It is a major shortcoming that if the
various services fail that the code will likely crash and the data will
become unstable. While much of the shard information is in persisted
containers there are limits... like the TTL for the AOL backups. This
would never work in a payment system where queueing needs to be as
close to 100% as money can buy.


Status
------
I'm about to upload the final changes. I have some notes from a parallel project that I was using to test various features in ZMQ.
(https://github.com/rbucker881/freelance_bug)


Conclusions
-----------
I'm fairly certain that we can all agree that ZMQ is cool and rock solid and cool. It solves a lot of problems but it creates a few less than that. Here's what I have:

- broker-less PUSH->PULL with a PUB->SUB response is efficient
- ZMQ supports many-to-many sockets with a twist
- the twist comes at the cost of a server app (aka handler) not being available but there is a effective queuing mechanism that simply postpones the transaction. All you need to do is a) watch for dupes; b) trace the transaction.

I do not particularly like the trade-off. It seems like a cop-out because it's behavior that should be selectable anyway. And I've posted DOC and code bugs with the ZMQ and PYZMQ teams.

The really good news... I have a project with a broker. That broker is about 200-500 lines of code depending on your PEP. By converting to brokerless I can remove a lot of code. (this broker is complicated.)

Thank you.

.END.