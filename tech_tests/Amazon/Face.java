package amazon.cards;

/** 
 * the Face class is a simple container class and is named for clarity
 * */
public class Face {

    private String face;

    public Face(String myFace) {
        this.face = myFace;
    }

    public String toString() {
        return face;
    }
}

// __END__



