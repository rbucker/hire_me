package amazon.cards;

// the card is a simple container
public class Card {
    private Suit suit;
    private Face face;

    public Card(Suit mySuit, Face myFace) {
        suit = mySuit;
        face = myFace;
    }

    public String toString() {
        if (suit == null) {
            return face.toString();
        } else {
            return face + " of " + suit;
        }
    }
}

// __END__

