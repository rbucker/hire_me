package amazon.cards;

import java.util.Vector;

public class UnoCards implements DeckType {

    private static UnoCards ref = null;

    public Vector<Card> shuffle() {
        // create a container to hold the cards
        Vector<Card> deck = new Vector<Card>();
        // get all the suits and faces
        Vector<Suit> suits = UnoCardSuits.getSuits().getAllSuits();
        Vector<Face> faces = UnoCardFaces.getFaces().getAllFaces();
        deck.clear();
        for(Suit s : suits) {
            for(Face f :faces) {
                deck.add(new Card(s,f));
            }
        }
        // add the special cards
        deck.add(new Card(null,new Face("Wild")));
        deck.add(new Card(null,new Face("Reverse")));
        deck.add(new Card(null,new Face("Skip")));
        return deck;        
    }

    public static UnoCards getDeck() {
        if (ref == null) {
            ref = new UnoCards();
        }
        return ref;
    }


}

// __END__
