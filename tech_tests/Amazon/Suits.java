package amazon.cards;

import java.util.Vector;

/**
 * The Suits class is a "singleton" that represents the Suits that a Card can be assigned
 * */
public class Suits {
    private static Suits ref = null;
    private static String[] suit_values = {"Hearts","Diamonds","Clubs","Spades"};
    private Vector<Suit> suits = null;

    private Suits() {
    }

    public static Suits getSuits() {
        if (ref == null) {
            ref = new Suits();
            ref.suits = new Vector<Suit>();
            for(String a : suit_values) {
                ref.suits.add(new Suit(a));
            }
        }
        return ref;
    }

    public Vector<Suit> getAllSuits() {
        return suits;
    }
}

// __END__




