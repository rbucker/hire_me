package amazon.cards;

import java.util.Vector;
import java.util.Random;

/**
 * This class represents a standard deck of playing cards. It could be modified
 *
 * */
public class DeckOfCards {
    // deck is a container for holding all of the cards in the deck.
    private Vector<Card> deck = new Vector<Card>();
    // instance of the random number generator
    private Random rand = new Random();

    /**
     * clear out the deck and then reshuffle the cards
     * */
    public void shuffle(DeckType deckref) {
        deck = deckref.shuffle();
    }

    /**
     * get one card from a random place in the deck. If the deck is empty or not shuffled then return null.
     * @return Card return the Card instance
     * */
    public Card deal_one_card() {
        int n = deck.size();
        if (n>0) {
            int i = rand.nextInt(n);
            return deck.remove(i);
        } 
        return null;
    } 

    /**
     * In addition to unit testing via junit or others; this main() will test this class.
     * @param args the command line args that java passes through to my app
     * */
    public static void main(String[] args) {
        System.out.println("DeckOfCards: unit tests");
        DeckOfCards deck = new DeckOfCards();
        
        Card card = deck.deal_one_card();
        if (card == null) {
            System.out.println("TEST1: the deck has not been shuffled yet and and we could not retrieve a card. OK");
        } else {
            System.out.println("TEST1 - ERROR: The deck has not been shuffled but we retrieved a card. ERROR");
        }
        
        System.out.println("TEST2: Shuffling the deck");
        deck.shuffle(PlayingCards.getDeck());
        System.out.println("TEST3: checking all of the cards");
        card = deck.deal_one_card();
        while(card != null) {
            System.out.println(card);
            card = deck.deal_one_card();
        }

        System.out.println("TEST4: Shuffling the deck");
        deck.shuffle(UnoCards.getDeck());
        System.out.println("TEST5: checking all of the cards");
        card = deck.deal_one_card();
        while(card != null) {
            System.out.println(card);
            card = deck.deal_one_card();
        }
        System.out.println("DeckOfCards: unit tests complete");
    }
}


// __END__
