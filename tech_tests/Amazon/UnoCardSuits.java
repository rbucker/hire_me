package amazon.cards;

import java.util.Vector;

/**
 * The Suits class is a "singleton" that represents the Suits that a Card can be assigned
 * */
public class UnoCardSuits {
    private static UnoCardSuits ref = null;
    private static String[] suit_values = {"Blue", "Red", "Yellow", "Green"};
    private Vector<Suit> suits = null;

    private UnoCardSuits() {
    }

    public static UnoCardSuits getSuits() {
        if (ref == null) {
            ref = new UnoCardSuits();
            ref.suits = new Vector<Suit>();
            for(String a : suit_values) {
                ref.suits.add(new Suit(a));
            }
        }
        return ref;
    }

    public Vector<Suit> getAllSuits() {
        return suits;
    }
}

// __END__




