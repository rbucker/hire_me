package amazon.cards;

import java.util.Vector;

public class PlayingCards implements DeckType {

    private static PlayingCards ref = null;

    public Vector<Card> shuffle() {
        // create a container to hold the cards
        Vector<Card> deck = new Vector<Card>();
        // get all the suits and faces
        Vector<Suit> suits = PlayingCardSuits.getSuits().getAllSuits();
        Vector<Face> faces = PlayingCardFaces.getFaces().getAllFaces();
        deck.clear();
        for(Suit s : suits) {
            for(Face f :faces) {
                deck.add(new Card(s,f));
            }
        }
        return deck;        
    }

    public static PlayingCards getDeck() {
        if (ref == null) {
            ref = new PlayingCards();
        }
        return ref;
    }
}

// __END__

