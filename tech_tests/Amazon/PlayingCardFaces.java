package amazon.cards;

import java.util.Vector;

/**
 * The Faces class is a "singleton" that represents the Faces that a Card can be assigned
 * */
public class PlayingCardFaces {
    private static PlayingCardFaces ref = null;
    private static String[] face_values = {"Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King"};
    private Vector<Face> faces = null;

    private PlayingCardFaces() {
    }

    public static PlayingCardFaces getFaces() {
        if (ref == null) {
            ref = new PlayingCardFaces();
            ref.faces = new Vector<Face>();
            for(String a : face_values) {
                ref.faces.add(new Face(a));
            }
        }
        return ref;
    }

    public Vector<Face> getAllFaces() {
        return faces;
    }
}

// __END__



