package amazon.cards;

/** 
 * the Suit class is a simple container class and is named for clarity
 * */
public class Suit {

    private String suit;

    public Suit(String mySuit) {
        this.suit = mySuit;
    }

    public String toString() {
        return suit;
    }
}

// __END__


