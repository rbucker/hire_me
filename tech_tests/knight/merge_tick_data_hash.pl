#!/usr/bin/env perl
use strict;
use warnings;

die "USAGE: merge.pl <file1> <file2> [file3...]" if @ARGV < 2;

my %cache;

foreach my $filename (@ARGV) {
  open my $fh, "<", $filename or die "Could not open '$filename' - $!";
  while(<$fh>) {
    next if substr($_,0,1) eq "/";
    $cache{$filename} = {filename=>$filename,fh=>$fh,line=>$_};
    last;
  }
}

die "ERROR: duplicate filenames on command line" if @ARGV != keys %cache;

while(1) {
  my $tcell;
  for my $filename (keys %cache) {
    if ( !$tcell || $cache{$filename}{line} lt $tcell->{line}) {
      $tcell = $cache{$filename}
    }
  }
  last if !$tcell;
  print $tcell->{line};
  delete $cache{$tcell->{filename}} if !($tcell->{line} = readline($tcell->{fh}));
}

__END__
