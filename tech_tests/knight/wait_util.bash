#!/usr/bin/env bash

if [ "${1}" = "" ]; then
    echo "info: usage: wait_util.bash <filename>"
    exit 1
fi

snooze=5
x=1
while [ $x -le 6 ]
do
  echo "info: waiting for ${1}"
  if [ -e $1 ]; then
    echo "info: done waiting"
    exit 0
  fi


  x=$(( $x + 1 ))
  echo "info: sleeping ${snooze} seconds"
  sleep ${snooze}
done




