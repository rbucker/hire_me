#!/usr/bin/env perl
use strict;
use warnings;


die "USAGE: merge.pl <file1> <file2> [file3...]" if $#ARGV < 1;

my @cache;

foreach my $filename (@ARGV) {
  open (my $fh, $filename) or die $!;
  my $line;
  while($line = readline($fh)) {
    last if substr($line,0,1) ne "/";
  }
  push(@cache, { handle=>$fh, row=>$line} );
}

while(1) {
  my $n;
  my $row = "A";
  for my $i (0..$#cache) {
    if ( $cache[$i]{row} lt $row) {
      $n = $i;
      $row = $cache[$i]{row};
    }
  }
  last if ($row eq "A");
  print $row;
  $cache[$n]{row} = readline($cache[$n]{handle});
  splice @cache, $n , 1 if !defined($cache[$n]{row});
}

###
