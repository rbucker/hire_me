use Clone qw(clone);

my %h1 = ( k1 => "v1"  );
my %h2 = ( k2 => \%h1  );
my %h3 = ( k3 => \%h2  );

#my %h4 = %h3;
my %h4 = %{ clone (\%h3) };

print "h3\n";
foreach my $i ( keys %h3 ) {
 print "$i => $h3{$i}\n";
 foreach my $j (keys %{$h3{$i}}) {
  print "  $j => $h3{$i}{$j}\n";
  foreach my $k ( keys %{$h3{$i}{$j}}) {
    print "    $k => $h3{$i}{$j}{$k}\n";
  }
 }
}

$h3{k3}{k2}{k1} = "v2";

print "h4\n";
foreach my $i ( keys %h4 ) {
 print "$i => $h4{$i}\n";
 foreach my $j (keys %{$h4{$i}}) {
  print "  $j => $h4{$i}{$j}\n";
  foreach my $k ( keys %{$h4{$i}{$j}}) {
    print "    $k => $h4{$i}{$j}{$k}\n";
  }
 }
}

