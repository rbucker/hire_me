#!/usr/bin/env perl

my @stack;
while(<STDIN>) {
    if (/foo/) {
        @stack = ();
        next;
    }
    push(@stack, $_);
    print shift(@stack) if @stack > 2;
}
print @stack;

###
