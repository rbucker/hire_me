#!/usr/bin/env perl -w

# %h1 = %h2 copies a reference from one has to ther other.
# meaning that any changes to %h1 and it's children are
# going to be reflected in %h2.  Try this:

my %h1 = ( fred=>'barney', wilma=>'betty', kids=>{bambam=>'pebbles'}, pets=>{dino=>'taz'});

{
print $h1{pets}->{dino};
print "\n";
my %h2 = %h1;
$h2{pets}->{dino} = 'onid';
print $h1{pets}->{dino};
print "\n";
}

# the assignment operator only build a new initial instance (the root hash)
# since pets and kids are references the reference values are copied as-is
# and therefore the new root instance is linked to the same pets and kids
# references. To avoid this problem, one needs to implement or use a deep 
# copy function.

sub deepcopy {
  my ($r) = @_;
  my $retval = {};
  foreach my $key (keys %$r) {
    my $val = $r->{$key};
    if (ref($val) eq ref("str")) {
        $retval->{$key} = $val;
    }
    else {
        $retval->{$key} = deepcopy($val)
    }
  }
  return $retval;
}


{
print "----with deepcopy---\n";
print $h1{pets}->{dino};
print "\n";
my $h2 = deepcopy(\%h1);
print %h1;
print "\n";
print %$h2;
print "\n";
$h2->{pets}->{dino} = 'bedrock';
print $h1{pets}->{dino};
print "\n";
}

# notice that in the second output section then dino was not re-assigned 'bedrock'
# as in the first example dino was assigned onid.

__END__
