#!/usr/bin/env lua 

function len (op) 
    if type(op) == "nil" then
        return 0
    elseif type(op) == "string" then
        return string.len(op)
    elseif type(op) == "table" then
        local count = 0
        for _ in pairs(op) do count = count + 1 end
        return count
    end
end

function trim1(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local argc = len(arg) - 2
assert(argc >= 2, "ERROR you need more than one filename on the command line")

local ticks = {}
for i, filename in pairs(arg) do
    if i >= 1 then
        local rdr = assert(io.open(filename))
        assert( rdr ~= nil, string.format("ERROR there was something with the input file %s", filename))
        local row;
        for trow in rdr:lines() do 
            if string.sub(trow,1,1) ~= "/" then
                row = trow
                break
            end
        end
        if len(row) > 0 then
            local tick_data = {filename=filename, rdr=rdr, row=row}
            ticks[filename] = tick_data
        end
    end
end

assert( argc == len(ticks), "ERROR you need more than one filename and no empty or dups on the command line")

while true do
    local tcell = {filename=nil, rdr=nil, row="A"}
    for _, cell in pairs(ticks) do
        if cell.row < tcell.row then
            tcell = cell
        end
    end
    if tcell.row == "A" then
        break
    end
    print(string.format("%s - %s", tcell.filename , trim1(tcell.row)))
    local trow = tcell.rdr:read("*L")
    if trow ~= nil then
        tcell.row = trow
        ticks[tcell.filename] = tcell 
    else
        ticks[tcell.filename] = nil;
    end
end

-- __END__
