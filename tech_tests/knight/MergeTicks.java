// The java version of the merge_tick problem

import java.lang.System;
import java.util.*;
import java.io.*;

public class MergeTicks {
    public static void main(String[] argv) throws Exception {
        if (argv.length <= 1) {
            throw new AssertionException("ERROR: at least 2 filenames arequired on the command line");
        }

        Ticks ticks = new Ticks();
        for( String filename: argv) {
                BufferedReader br = open(filename); 

                String strLine;
                while ((strLine = br.readLine()) != null)   {
                    if (!strLine.substring(0,1).equals("/")) {
                        ticks.add(new TickData(filename, br, strLine));
                        break;
                    }
                }
        }
        if (argv.length != ticks.size()) {
            throw new AssertionException("ERROR: either there are missing, duplicate filenames or empty files");
        }
        while( true ) {
            TickData tcell = new TickData(null, null, "A"); 
            for( TickData cell: ticks) {
                if (cell.line.compareTo(tcell.line) < 0) {
                    tcell = cell;
                }
            }
            if (tcell.line.equals("A")) {
                break;
            }
            System.out.printf("%s - %s\n", tcell.filename, tcell.line);
            tcell.line = tcell.br.readLine();
            if (tcell.line == null) {
                ticks.remove(tcell);
            }
        }
    }

    public static BufferedReader open(String filename) throws java.lang.Exception {
        FileInputStream fstream = new FileInputStream(filename);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        return(br);
    }
}

class AssertionException extends java.lang.Exception {
    public AssertionException(String err) {
        super(err);
    }
}

class Ticks extends HashSet<TickData> {
}

class TickData {
    public TickData(String filename, BufferedReader br, String line) {
        this.filename   = filename;
        this.br         = br;
        this.line       = line;
    }
    public String filename;
    public BufferedReader br;
    public String line;
}

// __END__
