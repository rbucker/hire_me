#!/usr/bin/env ruby

abort "ERROR at least 2 files need to be on the command line" if ARGV.size < 2

ticks = Hash.new
ARGV.each do|filename|
    f = File.new(filename)
    while (line = f.gets()) do
        if line[0,1] != "/" then
            ticks[filename] = {filename: filename, fh: f, line: line}
            break
        end
    end
end

abort "ERROR either one of the files is empty or the filename is duplicated" if ARGV.size != ticks.size

while true do
    tcell = {line: "A"}
    ticks.each_value do |cell|
        if cell[:line] < tcell[:line] then
            tcell = cell
        end
    end
    if tcell[:line] == "A" then
        break
    end
    puts tcell[:line]
    tcell[:line] = tcell[:fh].gets()
    if tcell[:line] == nil then
        ticks.delete(tcell[:filename])
    end
end
