#!/usr/bin/env python
import sys

assert len(sys.argv) > 2,  "USAGE: merge.pl <file1> <file2> [file3...]"

cache = {}
 
for i, filename in enumerate(sys.argv):
    if i < 1: continue
    fh = open(filename)
    for row in fh:
        if row[0] != "/":
            cache[filename] = {'filename':filename, 'rdr':fh, 'row':row}; break

assert len(sys.argv)-1 == len(cache), "ERROR: duplicate filenames on command line"
 
while len(cache):
    tcell = {'row':"A",}
    for cell in cache.values():
        if cell['row'] < tcell['row']: 
            tcell = cell
    print tcell['row'].strip()
    for row in tcell['rdr']:
        cache[tcell['filename']]['row'] = row; break
    else: del cache[tcell['filename']]

###
