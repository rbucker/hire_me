package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
)

type (
    tick_data struct {
        filename string
        rdr      *bufio.Reader
        row      string
    }
)

func main() {
    flag.Parse()
    if len(flag.Args()) < 2 {
        log.Fatal("ERROR need more than one filename on the command line\n")
    }
    var ticks = make(map[string]tick_data)
    for _, filename := range flag.Args() {
        file, err := os.Open(filename)
        if err != nil {
            log.Fatalf("ERROR could not open file %s for reading\n", filename)
        }
        rdr := bufio.NewReader(file)
        for trow, _, err := rdr.ReadLine(); err == nil; trow, _, err = rdr.ReadLine() {
            if trow[0] == '/' {
                continue
            }
            ticks[filename] = tick_data{filename, rdr, string(trow)}
            break
        }
    }
    if len(flag.Args()) != len(ticks) {
        log.Fatal("ERROR duplicate or empty filename on command line\n")
    }
    for {
        tcell := tick_data{"", nil, "A"}
        for _, cell := range ticks {
            if cell.row < tcell.row {
                tcell = cell
            }
        }
        if tcell.row == "A" {
            break
        }
        fmt.Printf("%s - %s\n", tcell.filename, tcell.row)
        trow, _, err := tcell.rdr.ReadLine()
        if err == nil {
            tcell.row = string(trow)
            ticks[tcell.filename] = tcell
            continue
        }
        delete(ticks, tcell.filename)
    }

}

// __END__
