def find_dup_in_list(mylist):
    """return a list of duplicated values from the input list
    """
    retval = []
    cache = {}
    for k in mylist:
        if k in cache:
            retval.append(k)
            continue
        cache[k] = 1
    return retval
    
print find_dup_in_list([1,2,3,'a','b','c'])
print find_dup_in_list([1,2,3,'a','b','c',1, 'a'])
