class CharacterNotFound(Exception):
    pass

def remove_char(ch, s):
    retval = ""
    for i in (xrange(0, len(s))):
        if s[i] == ch: continue
        retval += s[i]
    if retval == s:
        raise CharacterNotFound('?')
    return retval
        
print remove_char('a', 'a hello a world a')
print remove_char('z', 'hello world')
    
