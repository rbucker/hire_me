import sys
from PIL import Image

""" UNTESTED """

if __name__ == "__main__":
    """convert a gif file to png and save the file in the CWD/PWD
    """
    fname, ext = sys.argv[1].split('.')
    im = Image.open(sys.argv[1])
    transparency = im.info['transparency'] 
    im .save(fname+'.png', transparency=transparency)    
