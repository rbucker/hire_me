"""This code differs from the original only very slightly. The challenge I was attempting to 
resolve was the compression of the roman numerals during or after the conversion from decimal to roman.

There are a number of challenges here. Most notably Wikipedia reports that the rules are not standardized.
And clearly this is going to effect the process/interpretation in both directions. So while I added
some code for the int-roman and the decode should support it... it's still data dependent.
"""


from types import *

class BadRomanNumeral(Exception):
    pass

vals = {'I':1, 'IV':4, 'V':5, 'IX':9, 'X':10, 'XL':40, 'L':50, 'XC':90, 'C':100, 'CD':400, 'D':500, 'CM':900, 'M':1000 }
ord_val = ['M','CM','D','CD','C','XC','L','XL','X','IX','V','IV','I']

def int_2_roman_and_back(num):
    if type(num) is IntType:
        if num < 1: raise Exception('cannot be converted')
        retval = ""
        for tok in ord_val:
            val = vals[tok]
            n = int(num/val)
            if n>0:
                retval += tok * n
                num -= val * n
        return retval
    elif type(num) is StringType:
        # http://en.wikipedia.org/wiki/Roman_numerals
        # IVXLCDM
        retval = 0
        len_num = len(num)
        prev_val = 0
        for i in (xrange(0, len_num)):
            try:
                val = vals[num[i]]
                if val > prev_val:
                    retval -= (2*prev_val)
                    retval += val
                else:
                    retval += val
                prev_val = val
            except KeyError:
                raise BadRomanNumeral("char (%s) is not permitted" % (num[i]))
        return retval
    

print int_2_roman_and_back(1)
print int_2_roman_and_back(2)
print int_2_roman_and_back(4)
print int_2_roman_and_back(5)
print int_2_roman_and_back(8)
print int_2_roman_and_back(9)
print int_2_roman_and_back(10)
print int_2_roman_and_back(2012)
print int_2_roman_and_back(1999)
"""
print int_2_roman_and_back(1903)
print int_2_roman_and_back(999)
print int_2_roman_and_back(1996)
print int_2_roman_and_back(1999)

print int_2_roman_and_back("I")
print int_2_roman_and_back("II")
print int_2_roman_and_back("III")
print int_2_roman_and_back("IV")
print int_2_roman_and_back("V")
print int_2_roman_and_back("VI")
print int_2_roman_and_back("VII")
print int_2_roman_and_back("VIII")
print int_2_roman_and_back("IX")
print int_2_roman_and_back("IM")
print int_2_roman_and_back("MDCCCCLXXXXVIIII")
print int_2_roman_and_back("MCMXCIX")
print int_2_roman_and_back("MIM")

try:
    print int_2_roman_and_back('M3M')
except BadRomanNumeral as err:
    print "something was wrong with the test: %s" % (err,)
    
try:
    print int_2_roman_and_back(-1)
except BadRomanNumeral as err:
    print "something was wrong with the test: %s" % (err,)
except Exception as err:
    print "something bad happaned: %s" % (err,)

try:
    print int_2_roman_and_back(0)
except BadRomanNumeral as err:
    print "something was wrong with the test: %s" % (err,)
except Exception as err:
    print "something bad happaned: %s" % (err,)

"""